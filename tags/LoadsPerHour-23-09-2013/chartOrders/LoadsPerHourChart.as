﻿package  {
	/*

	Copyright (c) 2010 Neoris, All Rights Reserved
	
	@author   Andres Santos Becerra
	@contact  e-asantosb@neoris.com
	@project  RMSDispatcher
	INPEDIDO_CHANGE
	@internal
	
	*/
	import com.asblabs.juegoUtil;
	import com.asblabs.events.juegoUtilEvent;
	import com.asblabs.layouts.BaseLayout;
	import com.asblabs.primitives.FisixObject;
	import com.cartogrammar.drawing.DashedLine;
	import com.greensock.TweenLite;
	import com.greensock.events.TweenEvent;
	import com.hybrid.ui.ToolTip;
	import com.neoris.chart.ILoadsPerHourChart;
	import com.neoris.componentes.Barra;
	import com.neoris.componentes.ColumnaSnap;
	import com.neoris.componentes.CuadroGrafico;
	import com.neoris.componentes.ICuadroGrafico;
	import com.neoris.events.LoadsPerHourChartEvent;
	import com.neoris.mocks.ViewMock;
	import com.neoris.view.InicializadorVisual;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	[Event(name="inpedido_change",type="com.neoris.events.LoadsPerHourChartEvent")]
	[Event(name="cambio_hora",type="com.neoris.events.LoadsPerHourChartEvent")]
	[Event(name="full_detail",type="com.neoris.events.LoadsPerHourChartEvent")]
	[Event(name="abrir_mep",type="com.neoris.events.LoadsPerHourChartEvent")]
	[Event(name="ocupado",type="com.neoris.events.LoadsPerHourChartEvent")]
	[Event(name="libre",type="com.neoris.events.LoadsPerHourChartEvent")]
	
	public class LoadsPerHourChart extends juegoUtil implements ILoadsPerHourChart{
		
		/**
		 * Constructor
		 *
		 * <p>Carga todas las variables iniciales con valores</p>
		 *
		 */
		 
		private var lineaVehiculos:DashedLine;
		private var lineaCapacidad:DashedLine;
		private var lineaMaximaCapacidadPlanta:Sprite;
		private var _i:int;
		private var arraySpriteControl:Array=[];		
		public var lineaVehiculosPrev:Array;
		public var lineaCapacidadPrev:Array;
		public var lineaVehiculosNew:Array;
		public var lineaCapacidadNew:Array;
		
		private var _signosd:Array;
		private var _signosm:Array;
		
		private var tt:ToolTip;
		
		private var vistaMock:ViewMock;
		private var constructorCanvas:InicializadorVisual;
		
		//propiedades de las columnas y cuadritos
		
		public var anchoColumna:int;
		public var altoColumna:int;
		public var anchoCubo:int;
		public var altoCubo:int;
		public var multipleMove:Boolean;
		
		public var apuntarPedidos:Dictionary;
		public var descCuadritos:Dictionary;
		public var descCuadritos2:Dictionary;
		
		public var pendientes:Number=0;
		public var progreso:Number=0;
		public var completados:Number=0;
		public var cancelado:Number=0;
		public var confirmado:Number=0;
		
		public var statusHide:Boolean = true;
		private var dobleClick:Timer;
		private var cargaMaxima:int = 0;
		
		/*
		Colores se componen de la siguiente manera
		0-completed
		1-cancelled
		2-not started
		3-to be confirmed
		4-completed
		5-non-optimal assignation
		6-to jobsite
		7-on jobsite
		8-unloading
		9-simulated
		10-loading
		11-system proposal
		12-line Max plant capacity
		13-overbooking level
		14-vehicle availability
		*/
		public var colores:Array;
		
		public var ColumnasCompletas:Array;
		
		public function LoadsPerHourChart() {
			super();
			
			_signosm=[];
			_signosd=[];
			tt=new ToolTip();
			// constructor code
			
			/**
			 * Inicializador
			 *
			 * <p>Se pone este evento en caso de que se cargue de un swf externo.</p>
			 *
			 */
			 
			this.addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		/**
		 * Inicializador
		 *
		 * <p>Esta es la funcion primaria que inicializa todos los eventos del juego, esto permite
		 * saber lo que el usuario o el juego están haciendo</p>
		 *
		 */
		public var gthis:LoadsPerHourChart;
		public function init(evt:Event):void{
			multipleMove=false;
			gthis=this;
			this.GRAVITY=18;
			ColumnasCompletas=[];
			dobleClick=new Timer(300,1);
			dobleClick.addEventListener(TimerEvent.TIMER,fireClick);
			this.descCuadritos=new Dictionary();
			this.descCuadritos2=new Dictionary();
			this.apuntarPedidos=new Dictionary();
			this.UsarTecla(16,"separador");
			//this.ignorarNombreElemento("piso");
			//this.ignorarFisicaDeClase("com.neoris.componentes::Piso");
			//com.asblabs.juegoUtil.CENTRO=new Point(0,200);
			/**
			-Se dispara cuando se presiona la tecla de accion, por ejemplo la barra espaciadora
			ver:REFERENCIA:A
			**/
			//stage.addEventListener(KeyboardEvent.KEY_UP,resetearKey);
			//this.addEventListener(juegoUtilEvent.ACTIONKEY,teclaAccionPresionada);
			
			
			/**
			-se dispara cuando damos click en un objeto al que le pedimos al framework detecte el click
			ver:REFERENCIA:B
			**/
			this.addEventListener(juegoUtilEvent.CLICK2,mouseClick2);
			/**
			-se dispara cuando al arrastrar un objeto lo dejamos caer sobre otro, sin importar
			si es valido o invalida la colision
			
			ver:REFERENCIA:C
			**/
			this.addEventListener(juegoUtilEvent.COLLISSION,objetoColisiona);
			/**
			-es un tipo especial de colision, cuando los objetos no son cuadrados ni circulares,
			sino que necesitamos detectar la colision por ejemplo de dos lineas
			ver:REFERENCIA:D
			**/
			this.addEventListener(juegoUtilEvent.COMPLEXHIT,colisionCompleja);
			/**
			-se dispara cuando dos objetos han chocado, pero esos objetos no deben chocar, 
			por ejemplo en un juego en el q la pelota no debe caer a un charco de agua
			ver:REFERENCIA:E
			**/
			this.addEventListener(juegoUtilEvent.INVALIDHIT,golpeInvalido);
			
			/**
			-se dispara cuando dos objetos chocan y su choque es correcto, por ejemplo
			una flecha que le da a una manzana
			ver REFERENCIA:I
			**/
			this.addEventListener(juegoUtilEvent.VALIDHIT,golpeValido);
			/**
			-se dispara cuando el framework detecto que hemos llegado a un puntaje maximo o minimo que nosotros definimos
			ver:REFERENCIA:F
			**/
			this.addEventListener(juegoUtilEvent.MAXSCORE,maximoPuntajeAlcanzado);
			this.addEventListener(juegoUtilEvent.MINSCORE,minimoPuntajeAlcanzado);
			
			/**
			-se dispara cuando pasa un segundo en caso que hayamos definido un tiempo limite para nuestro juego
			ver referencia:G
			
			**/
			this.addEventListener(juegoUtilEvent.SECOND,unSegundo);
			
			/**
			-se dispara cuando nuestro reloj contador ha llegado a su fin
			ver:REFERENCIA:H
			**/
			this.addEventListener(juegoUtilEvent.TIMEOUT,tiempoTerminado);
			
			this.addEventListener(juegoUtilEvent.FLASHSINGLECYCLEOPERATION,cycles);
			
			this.addEventListener(juegoUtilEvent.STARTDRAG,function(evt:juegoUtilEvent){
								  //trace("cargar");
								  });
			this.addEventListener(juegoUtilEvent.STOPDRAG,detenidoDragBusy);
			
			this.addEventListener(juegoUtilEvent.MOVE_ITEM, onMoveItem)
			//this.addEventListener(Event.ENTER_FRAME, onOverItem);
			//elementos estaticos de las esquinas del gráfico
			//
			/****
			*
			* inicializa configuraciones ---- MOCK
			*
			*****/
			inicializarConfiguraciones();
		}
		private function resetearKey(evt:KeyboardEvent):void{
			this.multipleMove=false;
		}
		
		
		
		public var countBusy:int = 0;
		public var juegoUtilBusy:juegoUtilEvent;
		public var busy:Timer = new Timer(300, 1);
		private function detenidoDragBusy(evt:juegoUtilEvent):void{
			juegoUtilBusy = evt
			countBusy++;
			
			busy.addEventListener(TimerEvent.TIMER_COMPLETE, onCompleteBusy)
			busy.start();
		}
		private function onCompleteBusy(e:TimerEvent):void{
			if(countBusy == 2){
				countBusy = 0;	
				busy.removeEventListener(TimerEvent.TIMER_COMPLETE, onCompleteBusy)
			}else{
				countBusy = 0;	
				detenidoDrag(juegoUtilBusy);
			}
		}
		
		public function detenidoDrag(evt:juegoUtilEvent){			
			//this.removeEventListener(Event.ENTER_FRAME,this.seeHit)
			trace("detenidoDrag()");
			var conti:Boolean=true;
			  var nuevaHora:int=this.getNewHour((evt.dummy as CuadroGrafico)); /** aqui se define la nueva hora, es unica**/
			  trace("nuevaHora:"+nuevaHora);
			  var oldHour:int = (evt.dummy as CuadroGrafico).horarioPrev;
			trace("oldHour:"+oldHour);
			  
			  if(nuevaHora == -1000){
				  (evt.dummy as CuadroGrafico).y =45;
			  }else{
				  (evt.dummy as CuadroGrafico).y = 45//(listCuadritos[nuevaHora].list.length * (evt.dummy as CuadroGrafico).height) + 10
			  }
			  cleanDuplicateMain();
			  trace("columnaInactiva()");
			  if(!this.columnaInactiva(nuevaHora)){				  
			  		trace("columnaInactiva() ->"+(evt.dummy as CuadroGrafico).horario);
				  (evt.dummy as CuadroGrafico).horarioPrev=(evt.dummy as CuadroGrafico).horario;
				  (evt.dummy as CuadroGrafico).horario=nuevaHora;
				  (evt.dummy as CuadroGrafico).x=this.getChildByName("layout"+nuevaHora).x-(this.getChildByName("layout"+nuevaHora).width/2); /**se acomoda el cuadro justo en la columna de la hora**/
				  /***/
			  } else {
				  (evt.dummy as CuadroGrafico).estatico=true;
				  conti=false;
				  this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.INPEDIDO_CHANGE, evt.dummy as CuadroGrafico));
			  }
			  var diferencia:int=((evt.dummy as CuadroGrafico).horario-(evt.dummy as CuadroGrafico).horarioPrev);
			  var tmpHoraPrev:int
			  if(!this.multipleMove && conti){
				  for(var q in (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido]){
						//trace(this.apuntadorPedidos[numeroPedido][q]);
						trace((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"])
						//if( (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 0 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] !=4 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 6  && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"]!=7 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 8 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 10 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 100 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["isFix"] == false){
							//if( (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 0 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] !=4 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 6  && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"]!=7 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 8 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 10 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 100){
								if( (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["tipo"] != 0){
											var calc:int=0; /** esta es la nueva hora**/
											(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["estatico"]=false;
												(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["pasivo"]=false;
											var bigger:Boolean=false;
											calc=((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario+diferencia);
											tmpHoraPrev = (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario;
											//checkItemInLayer(evt.dummy as CuadroGrafico, nuevaHora, nuevaHora+calc)
											if(((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario+diferencia)>23){
												//calc=23;
												bigger=true;
											} else if(((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario+diferencia)<0){
												//calc=0;
												bigger=true;
											} else {
												//calc=((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario+diferencia);
											}
											if(!bigger){
												var inum:Number=this.getChildByName("layout"+calc).x-(this.getChildByName("layout"+calc).width/2);
												if((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].tipo>1 && (evt.dummy as CuadroGrafico)!=(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q] && !this.columnaInactiva((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario) && conti){
													if((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario >= 0 && (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario <= 23){
														TweenLite.to((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q],.5,{x:inum,y:(evt.dummy as CuadroGrafico).y,alpha:1, onComplete:onCompleteTween((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q], calc, (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario)});
													}else{
														var horarioInRange:int =  (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horarioInRange;
														if(horarioInRange > 23){
															horarioInRange = calc;
														}else if(horarioInRange < 0){
															horarioInRange = calc;
														}
														
														//(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["estatico"]=false;
														//(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["pasivo"]=false;
														TweenLite.to((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q],.5,{x:inum,y:(evt.dummy as CuadroGrafico).y,alpha:1, onComplete:onCompleteTween((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q], horarioInRange, (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario)});
														
													}
													/**
													 * registrar la posicion de los elementos cuando lleguen a la hora definida, para no utilizar un listener de fin de tween
													 * **/
													//checkItemInLayer((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q], calc, (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horarioPrev)
												}
											} else if(bigger && (evt.dummy as CuadroGrafico)!=(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]) {
												(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horarioInRange = (evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario;	
												TweenLite.to((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q],.5,{alpha:0});
												(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["estatico"]=true;
												(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]["pasivo"]=true;							
											}
											trace("columnaInactiva 2");
											if(!this.columnaInactiva((evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario) && (evt.dummy as CuadroGrafico)!=(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q]){
												(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horarioPrev = tmpHoraPrev;//(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario;
												(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario=calc;					
												trace('PREV ' +(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horarioPrev)
												trace('HORA ' +(evt.dummy as CuadroGrafico).apuntadorPedidos[(evt.dummy as CuadroGrafico).numeroPedido][q].horario)
											} 
						}
						
				}
				if((evt.dummy as CuadroGrafico).horario!=(evt.dummy as CuadroGrafico).horarioPrev){
					this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.CAMBIO_HORA,evt.dummy));
					onCompleteTween((evt.dummy as CuadroGrafico), (evt.dummy as CuadroGrafico).horario, (evt.dummy as CuadroGrafico).horarioPrev)
				}
			//	
			  }else{
				  if((evt.dummy as CuadroGrafico).horario!=(evt.dummy as CuadroGrafico).horarioPrev){
					this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.CAMBIO_HORA,evt.dummy, 0, 0, true));
					onCompleteTween((evt.dummy as CuadroGrafico), (evt.dummy as CuadroGrafico).horario, (evt.dummy as CuadroGrafico).horarioPrev)
				}
			  }
			  if(nuevaHora == -1000){
				  (evt.dummy as CuadroGrafico).y = 0;
			  }else{
				  (evt.dummy as CuadroGrafico).y = 0//(listCuadritos[nuevaHora].list.length * (evt.dummy as CuadroGrafico).height) + 10
			  }

			  cleanDuplicateMain();
		}
		/***************************************************************optimizacion de grafica*****************************************************************************************/
		private function onCompleteTween(item:CuadroGrafico, newHour:int, oldHour:int):void{
			if((newHour>=0 && newHour<=23) && (oldHour>=0 && oldHour<=23)){
				if(newHour != oldHour){
					listCuadritos[newHour].list = addItemCol(listCuadritos[newHour].list, item, newHour);
					listCuadritos[oldHour].list = removeItemCol(listCuadritos[oldHour].list, item);
				}
			}else{
				listCuadritos[newHour].list = addItemHideCol(listCuadritos[newHour].list, item, newHour);
			}
						
		}
		
		private var keys:* = {};
		private function cleanDuplicateMain():void{
			for(var i:int=0; i<this.listCuadritos.length; i++){
				var filteredArr:Array = this.listCuadritos[i].list.filter(removedDuplicates);
				 this.listCuadritos[i].list = filteredArr;
			}
		}
		private function removedDuplicates(item:*, idx:uint, arr:Array):Boolean {
			if (keys.hasOwnProperty(item.name)) {
				/* If the keys Object already has this property,
				return false and discard this item. */
				trace('borrado '+ item.name)
				return false;
			} else {
				/* Else the keys Object does *NOT* already have
				this key, so add this item to the new data
				provider. */
				keys[item.name] = item;
				return true;
			}
		}
		
		private var flagMove:Boolean = false;
		public function onMoveItem(e:juegoUtilEvent):void{
			//checkItemInLayer(e.dummy as CuadroGrafico);
			var modelSingleton:mainSingleton = mainSingleton.getInstance();
			modelSingleton.origenItem = (e.dummy as CuadroGrafico);
			flagMove = true
		}
		/*
		private function onOverItem(e:Event):void{
			if(flagMove == false){
				return;
			}
		
			var modelSingleton:mainSingleton = mainSingleton.getInstance();
			try{
				for each (var hour:Object in this.listCuadritos){
					for(var item:int=1; item<hour.list.length; item++){
						if(modelSingleton.origenItem.name != hour.list[item][1].name){
							if(modelSingleton.origenItem.hitTestObject(hour.list[item][1]) || hour.list[item][1].hitTestObject(modelSingleton.origenItem)){
								hour.list[item][1].y=  modelSingleton.origenItem.y-(hour.list[item][1].height-3);
							}						
						}
					}
				}
			}catch(e:Error){
				trace("ERROR LINEA 393 LoadsPerHourChart")
			}
		}
		*/
		
		
		private function checkItemInLayer(item:CuadroGrafico, newHour:int=1000, oldHour:int=1000):void{
			var cuadrito:CuadroGrafico = item
			origenItem = item;
			
			var flagDetectCollision:Boolean = false;
			for each(var obj:Object in this.listCuadritos){
				for (var i:int=0; i<obj.list.length; i++){
					if(cuadrito.hitTestObject(obj.list[i][1])){
						var horaOld:int
						if(newHour == 1000){							
							if(Number(cuadrito.horario) != Number(obj.list[i][1].horario)){
								horaOld = cuadrito.horario;
								flagDetectCollision = true;
								obj.list = addItemCol(obj.list, cuadrito, obj.list[i][1].horario)
								listCuadritos[horaOld].list = removeItemCol(listCuadritos[horaOld].list, cuadrito)
							}
						}
					}
				}
			}
		}
		private function addItemCol(array:Array, addValue:CuadroGrafico, hora:int):Array{
			var flagExistItem:Boolean = false;
			var outputArray:Array = new Array();
			for( var i:Number = 0; i < array.length; i++){
				if( array[i][1].name == addValue.name){
					flagExistItem = true;
				}else{
					outputArray.push([array[i][0], array[i][1]])
				}
			}
			if(flagExistItem == false){
				var val:CuadroGrafico = addValue
				addValue.horario = hora;
				outputArray.push([0, val]);
			}
			return outputArray;
		}
		
		private function addItemHideCol(array:Array, addValue:CuadroGrafico, hora:int):Array{
			var outputArray:Array = new Array();
			for( var i:Number = 0; i < array.length; i++){
				outputArray.push([array[i][0], array[i][1]])
			}
			return outputArray;
		}
		
		private function removeItemCol(array:Array, removeValue:CuadroGrafico):Array{
			var outputArray:Array = new Array();
			for( var i:Number = 0; i < array.length; i++){
				if(array[i][1].name != removeValue.name){
					outputArray.push([array[i][0], array[i][1]]);
				}
			}
			return outputArray;
		}		
		public function cleanReferenceItem():void{
			trace(this.listCuadritos.length)
			for(var i:int=0; i<this.listCuadritos.length; i++){
				listCuadritos[i].list = listCuadritos[i].list.slice(0,1)  
			}
			trace(listCuadritos)
		}
		private var origenItem:CuadroGrafico;		
		public function showMe(view:Boolean):void{
			this.visible = view;
			this.statusHide = view;
		}
		public function getOrigenItem():Object{
			var modelSingleton:mainSingleton = mainSingleton.getInstance();
			return modelSingleton.origenItem;
		}
		public function setDateQuery(date:Date):void{
			super.dateQuery = date;
		}
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**************************************************************optimizacion de grafica*****************************************************************************************/
		
		
		public function coloresStatus(completed:uint=0x54FF54,cancelled:uint=0xff0000,notStarted:uint=0x00ffff,toBeConfirmed:uint=0xffff00,completedWithDelay:uint=0xff6600,nonOptimalAssignation:uint=0x666666,toJobsite:uint=0x336600,onJobsite:uint=0x339900,unLoading:uint=0x33cc00,simulated:uint=0x00d1ff,loading:uint=0x999999,systemProposal:uint=0x663399,lineMaxPlantCapacity:uint=0x66ccff,overbookingLevel:uint=0xcc6600,vehicleAvailability:uint=0XCC00FF):void{
			colores=[completed,cancelled,notStarted,toBeConfirmed,completedWithDelay,nonOptimalAssignation,toJobsite,onJobsite,unLoading,simulated,loading,systemProposal,lineMaxPlantCapacity,overbookingLevel,vehicleAvailability];
		}
		public function inicializarConfiguraciones(wColumna:int=12,hColumna:int=275,wCubo:int=12,hCubo:int=19):void{
			this.anchoColumna=wColumna;
			this.altoColumna=hColumna;
			this.anchoCubo=wCubo;
			this.altoCubo=hCubo;
			coloresStatus();
			/****
			*
			* inicia creacion de elemento visual
			*
			*****/
			this.constructorCanvas=new InicializadorVisual(this);
			/****
			*
			* inicia creacion de gráfica ---- MOCK
			*
			*****/
			//this.vistaMock=new ViewMock(this);
		}
		private function cycles(evt:juegoUtilEvent):void{
			(this.getChildByName("piso") as FisixObject).estatico=true;
			if(this.lineaCapacidadPrev!=null){
				for(var q:int=0;q<this.lineaCapacidadPrev.length;q++){
					if(this.lineaCapacidadPrev[q]<this.lineaCapacidadNew[q] && this._signosm[q]==1){
						this.lineaCapacidadPrev[q]+=.2;
					} else if(this.lineaCapacidadPrev[q]>this.lineaCapacidadNew[q] && this._signosm[q]==-1){
						this.lineaCapacidadPrev[q]+=-.2;
					} else {
						this.lineaCapacidadPrev[q]=this.lineaCapacidadNew[q];
					}
				}
				this.capacidadMaxima(this.lineaCapacidadPrev);
				//this.linea(this.lineaCapacidadPrev);
			}
			if(this.lineaVehiculosPrev!=null){
				for(var qa:int=0;qa<this.lineaVehiculosPrev.length;qa++){
					if(this.lineaVehiculosPrev[qa]<this.lineaVehiculosNew[qa] && this._signosd[qa]==1){
						this.lineaVehiculosPrev[qa]+=.2;
					} else if(this.lineaVehiculosPrev[qa]>this.lineaVehiculosNew[qa] && this._signosd[qa]==-1){
						this.lineaVehiculosPrev[qa]+=-.2;
					} else {
						this.lineaVehiculosPrev[qa]=this.lineaVehiculosNew[qa]
					}
				}
				this.vehiculosDisponibles(this.lineaVehiculosPrev);
			}
		}
		
		private function mouseClick(evt:MouseEvent):void{
			
		}
		public function teclaAccionPresionada(evt:juegoUtilEvent):void{
			this.multipleMove=true;
		}
		public function mouseClick2(evt:juegoUtilEvent):void{
			
		}
		public function objetoColisiona(evt:juegoUtilEvent):void{
			
		}
		public function colisionCompleja(evt:juegoUtilEvent):void{
			
		}
		public function golpeInvalido(evt:juegoUtilEvent):void{
			
		}
		public function golpeValido(evt:juegoUtilEvent):void{
			
		}
		public function maximoPuntajeAlcanzado(evt:juegoUtilEvent):void{
			
		}
		public function minimoPuntajeAlcanzado(evt:juegoUtilEvent):void{
			
		}
		public function unSegundo(evt:juegoUtilEvent):void{
			
		}
		public function tiempoTerminado(evt:juegoUtilEvent):void{
			
		}
		public function getColumna(hora:int):Array{
			var horario:Array=[];
			for(var e:int=0;e<this.numChildren;e++){
				if((this["layout"+hora] as DisplayObject).hitTestObject(this.getChildAt(e))){
					if(this.getChildAt(e) is CuadroGrafico){
						horario.push({tipo:this.getChildAt(e),pedido:this.getChildAt(e)["numeroPedido"],horaOriginal:this.getChildAt(e)["horarioOriginal"],horaNueva:this.getChildAt(e)["horario"],minutos:this.getChildAt(e)["minutos"],posicion:this.getChildAt(e)["posicion"]});
					}
				}
			}
			return horario;
		}
		public function crearColumna(planta:String, plantaOptima:String, elementos:int,columna:int,tipoPedido:int,numeroPedido:Number,minutos:int,posicion:Number,colorFill:String="0xcccccc",columna1ToolTip:String="",columna2ToolTip:String="",alturaCaida:Number=-30,isFixed:Boolean=false, titleToolTip:String='Order'):ICuadroGrafico{
			var cuadrito:CuadroGrafico;
			for(var q:int=0;q<elementos;q++){
				    var colorBorder:uint = 0xffffff;
					cuadrito=new CuadroGrafico(this.anchoCubo,this.altoCubo,colorFill, colorBorder);
					cuadrito.posicion=posicion;
					cuadrito.minutos=minutos;
					cuadrito.tituloTooltip = titleToolTip;
					cuadrito.col1=columna1ToolTip;
					cuadrito.col2=columna2ToolTip;
					this.descCuadritos[cuadrito]=columna1ToolTip;
					this.descCuadritos2[cuadrito]=columna2ToolTip;
					this.addChild(cuadrito);
					cuadrito.doubleClickEnabled=true;
					cuadrito.tipo=tipoPedido;
					cuadrito.horario=columna;
					cuadrito.plantaOptima = plantaOptima;
					cuadrito.plantaActual = planta;
					
					if(planta == plantaOptima || numeroPedido==0){
						cuadrito.drawPattern(false)
					}else{
						cuadrito.drawPattern(true);
						//cuadrito.drawColorBorder();
					}
					
					cuadrito.numeroPedido=numeroPedido;
					if(this.apuntarPedidos[cuadrito.numeroPedido]==null){
						this.apuntarPedidos[cuadrito.numeroPedido]=new Array();
					}
					this.apuntarPedidos[cuadrito.numeroPedido].push(cuadrito);
					cuadrito.apuntadorPedidos=apuntarPedidos;
					//if(tipoPedido>1 && !this.getChildByName("layout"+columna)["hasPast"] && !isFixed){
					if(tipoPedido>1 && !this.getChildByName("layout"+columna)["hasPast"]){
						this.Arrastrable(cuadrito);
					}else{
						cuadrito.isFix = true;
					}
					cuadrito.x=((this.anchoColumna+5)*columna)+this.anchoColumna;
					cuadrito.y=-(alturaCaida+(cuadrito.height*q)+45);
									
					/*cuadrito.addEventListener(MouseEvent.MOUSE_DOWN,mostrarFullDetailDown);
					cuadrito.addEventListener(MouseEvent.MOUSE_UP,mostrarFullDetailUp);
					cuadrito.addEventListener(MouseEvent.MOUSE_UP,mostrarFullDetailUp);*/
					
					cuadrito.addEventListener(MouseEvent.MOUSE_OVER,mostrarToolTip);					
					cuadrito.addEventListener(MouseEvent.MOUSE_OUT,ocultarToolTip);	
					cuadrito.addEventListener(MouseEvent.MOUSE_DOWN,preMep);					
					cuadrito.addEventListener(MouseEvent.DOUBLE_CLICK,onFullDetailUp);
										
					cuadrito.rayas.addEventListener(MouseEvent.MOUSE_OVER,mostrarToolTip);
					cuadrito.rayas.addEventListener(MouseEvent.MOUSE_OUT,ocultarToolTip);
					cuadrito.rayas.addEventListener(MouseEvent.MOUSE_DOWN,preMep);

					cuadrito.rayas.addEventListener(MouseEvent.DOUBLE_CLICK,onFullDetailUp);
				
				//}
				/*******optimizacion de codigo************/
				var existItem:Boolean = false
				var target:DisplayObject;
				
				for each(var obj:Object in this.listCuadritos){
					if(obj.id == columna){						
						target = this.getChildByName(cuadrito.name);
						for(var i:int=1; i < obj.list.length; i++){	
							//if(this.getChildIndex(target) == obj.list[i][0]){
							if(target.name == CuadroGrafico(obj.list[i][1]).name){
								existItem = true;
								break;
							}
						}
						if(existItem == false){					
							obj.list.push([this.getChildIndex(target), cuadrito]);
						}
					}
					
				}				
				
				/*******optimizacion de codigo************/
			}
			
			if(elementos==1){
				return cuadrito;
			} else {
				return null;
			}
		}
		
		private var cuad:CuadroGrafico;
		private var contTimer:int;
		private function mostrarFullDetailDown(evt:MouseEvent):void{
			cuad=evt.target as CuadroGrafico;
			contTimer=flash.utils.getTimer();
		}
		private function mostrarFullDetailUp(evt:MouseEvent):void{
			if((flash.utils.getTimer()-contTimer)<300){
				trace("full detail");
				dobleClick.start();
			}
		}
		private function mostrarMep(evt:MouseEvent):void{
			trace("MEP")
			dobleClick.stop();
			dobleClick.reset();
			this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.ABRIR_MEP,(evt.target as CuadroGrafico)));
		}
		private function fireClick(evt:TimerEvent):void{
			if(cuad!= null){
				//this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.FULL_DETAIL,cuad));
			}
		}
		
		private function onFullDetailUp(evt:MouseEvent):void{
			this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.FULL_DETAIL, (evt.target as CuadroGrafico)));
		}
		private function preMep(evt:MouseEvent):void{
			//if(evt.target is CuadroGrafico){
			try{
				
				var cuadro:CuadroGrafico = (evt.currentTarget as CuadroGrafico);
				
				//var dateQuery:Date = new Date(cuadro.date);
				
				super.dateQuery = new Date(super.dateQuery.fullYear, super.dateQuery.month, super.dateQuery.date);
				var dateActual:Date = new Date();
				dateActual = new Date(dateActual.fullYear, dateActual.month, dateActual.date);
				
				if(super.dateQuery < dateActual){
					this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.ALERT_UNMOVE,cuadro));
					return
				}
				//if(cuadro.tipo>1 && cuadro.tipo<99 && cuadro.isFix== false /*****falta especificar cuando no es asignada*****/){
				if(cuadro.tipo>1 /*****falta especificar cuando no es asignada*****/){
					if(cuadro.tipo==4  || cuadro.tipo==6 || cuadro.tipo==7 || cuadro.tipo==8 || cuadro.tipo==10){
						//this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.ALERT_UNMOVE,cuadro));
						//return;
					}  
					this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.PRE_MEP,cuadro));
				} else {
					this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.ALERT_UNMOVE,cuadro));
				}
			}


catch(e:Error){}
				//this.dispatchEvent(new LoadsPerHourChartEvent(LoadsPerHourChartEvent.ABRIR_MEP,(evt.target as CuadroGrafico)));
			//}
		}
		
		
		
		public function vehiculosDisponibles(vehiculos:Array):void{
			if(this.ColumnasCompletas.length==0){
				   return;
			}
			//this.lineaVehiculosNew=this.lineaVehiculosPrev=vehiculos;
			//la linea no es un componete tipo CartesianObject
			//usamos sistema cartesiano normal de Flash
			if(lineaVehiculos!=null){
				try{
					this.removeChild(lineaVehiculos);
				} catch(e){
					
				}
			}
			lineaVehiculos=new DashedLine(4,colores[12]);
			//trace((layout1 as BaseLayout).posicionesY(this,true));
			var todasPosicionesx:Array=[];
			var todasPosicionesy:Array=[];
			for(var v:int=0;v<ColumnasCompletas.length;v++){
				var posicionesx:Number=(this.getChildByName("layout"+v) as BaseLayout).x-((this.getChildByName("layout"+v) as BaseLayout).width/2);
				var posicionesy:Number=380-(this.anchoColumna*vehiculos[v]);//(this["layout"+v] as BaseLayout).posicionesY(this,true)[0];
				//y = (altodeBarra/vehiculosPlanta)*(vehiculosDisponible)
				trace(380)
				trace(cargaMaxima)
				trace(vehiculos[v])
				//var posicionesy:Number = 0;//((380/cargaMaxima) * vehiculos[v])+380
				//trace(posicionesy);
				todasPosicionesx.push(posicionesx);
				todasPosicionesy.push(posicionesy);
				
				todasPosicionesx.push(posicionesx+this.anchoColumna);
				todasPosicionesy.push(posicionesy);
				
			}
			lineaVehiculos.moveTo(todasPosicionesx[0],todasPosicionesy[0]);
			for(var vi:int=0;vi<todasPosicionesx.length;vi++){
				lineaVehiculos.lineTo(todasPosicionesx[vi],todasPosicionesy[vi]);
			}
			this.addChild(lineaVehiculos)
		}
		
		
		public function capacidadMaxima(capacidad:Array):void{
			if(this.ColumnasCompletas.length==0){
				   return;
			}
			//this.lineaCapacidadNew=this.lineaCapacidadPrev=capacidad;
			//la linea no es un componete tipo CartesianObject
			//usamos sistema cartesiano normal de Flash
			if(lineaCapacidad!=null){
				//TweenLite.to(lineaCapacidad
				//this.lineaCapacidad.removeEventListener(MouseEvent.MOUSE_OVER,mostrarToolTip);
				try{
					this.removeChild(lineaCapacidad);
				} catch(e){
					
				}
				
			}
			lineaCapacidad=new DashedLine(4,colores[13]);
			//trace((layout1 as BaseLayout).posicionesY(this,true));
			var todasPosicionesx:Array=[];
			var todasPosicionesy:Array=[];
			//TweenLite.to();
			
			for(var v:int=0;v<this.ColumnasCompletas.length;v++){
				var posicionesx:Number=(this.getChildByName("layout"+v) as BaseLayout).x-((this.getChildByName("layout"+v) as BaseLayout).width/2);
				var posicionesy:Number=  ((280) - (((280/cargaMaxima)*(capacidad[v])))) + 100  //((cargaMaxima*capacidad[v])-380)+380;
				//((380/cargaMaxima) * vehiculos[v])+380
				
				todasPosicionesx.push(posicionesx);
				todasPosicionesy.push(posicionesy);
				//trace(todasPosicionesx.length)
				todasPosicionesx.push(posicionesx+this.anchoColumna);
				todasPosicionesy.push(posicionesy);
				
				
				
			}
			lineaCapacidad.moveTo(todasPosicionesx[0],todasPosicionesy[0]);
			for(var vi:int=0;vi<todasPosicionesx.length;vi++){
				lineaCapacidad.lineTo(todasPosicionesx[vi],todasPosicionesy[vi]);
			}
			this.addChild(lineaCapacidad);
		}
		public function capacidadMaximaAnim(capacidad:Array):void{
			this.lineaCapacidadPrev=this.lineaCapacidadNew;
			this.lineaCapacidadNew=capacidad;
			try{
				for(var c:int=0;c<this.lineaCapacidadPrev.length;c++){
					if(this.lineaCapacidadPrev[c]>this.lineaCapacidadNew[c]){
						this._signosm[c]=-1
					} else {
						this._signosm[c]=1;
					}
				}
			} catch(e){
				
			}
		}
		public function vehiculosDisponiblesAnim(vehiculos:Array):void{
			this.lineaVehiculosPrev=this.lineaVehiculosNew;
			this.lineaVehiculosNew=vehiculos;
			for(var c:int=0;c<this.lineaVehiculosPrev.length;c++){
				if(this.lineaVehiculosPrev[c]>this.lineaVehiculosNew[c]){
					this._signosd[c]=-1;
				} else {
					this._signosd[c]=1;
				}
			}
		}
		private function mostrarToolTip(evt:MouseEvent):void{
			tt.align = "center";
			tt.hook = true;
			tt.tipWidth=300;
			tt.cornerRadius = 0;
			tt.bgAlpha = 1;
			//tt.autoSize = true;
			try{
				tt.show( (evt.target as DisplayObject), (evt.target as CuadroGrafico).tituloTooltip+": "+(evt.target as CuadroGrafico).numeroPedido, this.descCuadritos[evt.target],this.descCuadritos2[evt.target]);
			} catch(e){
				
			}
		}
	
		private function ocultarToolTip(evt:MouseEvent):void{
			//tt.hide();
		}
		public function limpiar():void{
			var cont:int=0;
			if(this.lineaMaximaCapacidadPlanta!=null){
			   lineaMaximaCapacidadPlanta.graphics.clear();
			}
			try{
				
				this.removeChild(lineaVehiculos);
				this.removeChild(lineaCapacidad);
			} catch(e){
				
			}
			
			for(var g:int=this.numChildren-1;g>-1;g--){
				if(this.getChildAt(g) is CuadroGrafico){
					this.removeChild(getChildAt(g));
				}
			}
			cleanReferenceItem() /**Optimizacion grafica**/
			
			limpiarLineas()
				
		}
		
		public function limpiarLineas():void{
			
			
			//'linea'+yLinea;
			/*var child:DisplayObject;
			for(var yLinea:int=0;yLinea<=23;yLinea++){
				if(getChildByName('linea'+yLinea)!=null){
					 child = getChildByName('linea'+yLinea);
					 removeChild(child);
					
				}*/
			if(arraySpriteControl!=null ){
					for (var y :int=0;y<=arraySpriteControl.length;y++){
							if(arraySpriteControl[y]!=null  && arraySpriteControl[y] is Sprite ){
							//	arraySpriteControl[y].parent.removeChild(arraySpriteControl[y])
								arraySpriteControl[y].graphics.clear();
							}
								
							
						}
					}
			}
		
			
			
			
			
		
		public function borrarPorPedido(numeroPedido:int):void{			
			for(var g:int=this.numChildren-1;g>-1;g--){
				if(this.getChildAt(g) is CuadroGrafico){
					var cuadro:CuadroGrafico=this.getChildAt(g) as CuadroGrafico;
					if(cuadro.numeroPedido==numeroPedido){
						//if( cuadro.tipo != 0 && cuadro.tipo != 4 && cuadro.tipo != 100 && cuadro.isFix == false){
						if( cuadro.tipo != 0 && cuadro.tipo != 4){	
							this.listCuadritos[cuadro.horarioOriginal].list = removeItemCol(this.listCuadritos[cuadro.horarioOriginal].list, cuadro); /*** optimizacion de grafica**/
							this.removeChild(cuadro);
						}
					}
				}
			}
		}
		public function borrarPorPosicion(posicion:int):void{			
				for(var g:int=this.numChildren-1;g>-1;g--){
					if(this.getChildAt(g) is CuadroGrafico){
						var cuadro:CuadroGrafico=this.getChildAt(g) as CuadroGrafico;
						//if( cuadro.tipo != 0 && cuadro.tipo != 4 && cuadro.tipo != 100 && cuadro.isFix == false){
						if( cuadro.tipo != 0 && cuadro.tipo != 4){
							if(cuadro.posicion==posicion){
								this.listCuadritos[cuadro.horarioOriginal].list = removeItemCol(this.listCuadritos[cuadro.horarioOriginal].list, cuadro); /*** optimizacion de grafica**/
								this.removeChild(cuadro);
							}
						}
					}
				}
		}
		public function obtenerPorPedido(numeroPedido:int):Array{
			var arr:Array=[];
			var cont:int=0;
			
			for(var g:int=0;g<this.numChildren;g++){
				if(this.getChildAt(g) is CuadroGrafico){
					var cuadro:ICuadroGrafico=this.getChildAt(g) as ICuadroGrafico;
					//if(cuadro["tipo"] != 0 && cuadro["tipo"] != 4 && cuadro["tipo"] != 100 && cuadro["isFix"] == false){
					if(cuadro["tipo"] != 0 && cuadro["tipo"] != 4){
						if(cuadro["numeroPedido"]==numeroPedido){
							arr.push(cuadro);
						}
					}
				}
			}
			
			return arr;
		}
		public function obtenerCuadro(posicioni:int):ICuadroGrafico{
			var cu:ICuadroGrafico=null;
			for(var g:int=0;g<this.numChildren;g++){
				if(this.getChildAt(g) is CuadroGrafico){
					var cuadro:ICuadroGrafico=this.getChildAt(g) as ICuadroGrafico;
					if(cuadro["posicion"]==posicioni){
						cu=cuadro;
					}
				}
			}
			return cu;
		}
		public function maximumPlantCapacity(valor:int):void{
			if(!this.lineaMaximaCapacidadPlanta){
				this.lineaMaximaCapacidadPlanta=new Sprite();
				addChild(lineaMaximaCapacidadPlanta);
			} else {
				lineaMaximaCapacidadPlanta.graphics.clear();
			}
			lineaMaximaCapacidadPlanta.graphics.lineStyle(4,colores[12]);
			lineaMaximaCapacidadPlanta.graphics.moveTo(this.anchoColumna/2,380-((this.altoCubo+3)*valor));
			lineaMaximaCapacidadPlanta.graphics.lineTo(((this.anchoColumna+5)*23)+this.anchoColumna,380-((this.altoCubo+3)*valor));
		}
		public function sapHoraLocal(hora:String):void{
			//formato "hh:mm:ss"
			//decolorear las horas de las columnas
			var hour:Number=Number(hora.substr(0,2));
			for(var f:int=0;f<hour;f++){
				this.getChildByName("layout"+f)["concluida"]();
				this.getChildByName("layout"+f)["hasPast"]=true;
			}
		}
		public function forzarHora(pedido:Number,posicion:Number, newHour:Number, oldHour:Number):void{
			
			
			for each(var cuadrito:Object in this.listCuadritos){
				for(var i:int=1; i< cuadrito.list.length; i++){		
					if(String((cuadrito.list[i][1] as CuadroGrafico).numeroPedido)+ String((cuadrito.list[i][1] as CuadroGrafico).posicion) == String(pedido)+String(posicion)){
						if(newHour >= 0 && newHour < 24){
							(cuadrito.list[i][1] as CuadroGrafico).x=this.getChildByName("layout"+newHour).x-(this.getChildByName("layout"+newHour).width/2);
							//(cuadrito.list[i][1] as CuadroGrafico).horario=newHour;
							try{
								onCompleteTween((cuadrito.list[i][1] as CuadroGrafico), newHour, oldHour)
							}catch(e:Error){trace("ERROR EN LINeA 869 LoadPerHourChart")}
							//cuadrito.list[i][1].alpha = 1;
						}else{
							//cuadrito.list[i][1].alpha = 0;
						}						
						break;
					}
				}
			}
		}
		public function getNewHour(cuadrito:CuadroGrafico):int{			
			var num:int=-1000;
			if(cuadrito != null){
				for(var g:int=0;g<24;g++){
					if(cuadrito.hitTestObject(this.getChildByName("layout"+g))){
						num=g;
					}
				}
			}
			return num;
			
		}
		private function columnaInactiva(num:int):Boolean{
			var comprobacion:Boolean=true;
			if(num<0 || num>23){
				comprobacion=false;
			} else {
				comprobacion=getChildByName("layout"+num)["hasPast"];
			}
			if(num==-1000){
				comprobacion=true;
			}
			return comprobacion;
		}
		public function capacidadMaximaDeCargas(cargas:int):void{
			cargaMaxima = cargas;
			this.altoCubo=(this.altoColumna/cargas)//+1;
			for(var wi:int=this.numChildren-1;wi>-1;wi--){
				if(this.getChildAt(wi).name.indexOf("cargas_")!=-1){
					this.removeChildAt(wi);
				}
			}
			for(var wii:int=0;wii<cargas;wii++){
				var texto1:TextField=new TextField();
				texto1.text=String(wii);
				texto1.name="cargas_"+wii
				this.addChild(texto1);
				texto1.width = 15
				texto1.x=0 - texto1.width + 8;
				texto1.y=370-(wii*(this.altoCubo+3));
				var tf1:TextFormat=new TextFormat();
				    tf1.font = "Arial"
					tf1.size = 9;
					tf1.color = 0x000000;
					tf1.bold = true;
					tf1.align = 'right';
				texto1.setTextFormat(tf1);
			}
		}
		
		
		public function colocaVehiculosDispLine(vehicleDispArray:Array):void{
			
			
			vehiculosLineas(vehicleDispArray);
		}
		
		
		private function vehiculosLineas(vehicleDispArray:Array):void{
			/*if(!this.lineaMaximaCapacidadPlanta){*/
				var linea:Sprite
				var xl:Number=0;
				var yl:Number=0;
				for(var yLinea:int=0;yLinea<vehicleDispArray.length;yLinea++){
						var valor:int= Number(vehicleDispArray[yLinea]);
						if(yLinea==0){
							xl= (this.anchoColumna+2)/2;
							yl= 380-((this.altoCubo+3)*valor);
						}else{
							xl=xl+7; //spacio
							yl= 380-((this.altoCubo+3)*valor);
						}
						linea=new Sprite();
						linea.name= 'linea'+yLinea;

						if(valor<=20 && valor>=0){
							
							addChild(linea);
							this.arraySpriteControl.push(linea);
							if(valor>=0){
								yl-3;
							}
						}
						var color:uint= 0xCC0000
						linea.graphics.lineStyle(4,color);
						linea.graphics.moveTo(xl,yl);
						xl=xl+10;
						linea.graphics.lineTo(xl,yl);
					
				}
				
		}
		
	
			
		
	}
	
	
}
