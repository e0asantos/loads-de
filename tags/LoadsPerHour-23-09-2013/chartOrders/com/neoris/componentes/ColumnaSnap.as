﻿package com.neoris.componentes{
	
	import flash.display.Sprite;
	import com.asblabs.layouts.VerticalLayout;
	import flash.geom.Rectangle;
	

	public class ColumnaSnap extends VerticalLayout{
		
		private var column:Sprite;
		public var hasPast:Boolean;
		private var dimensiones:Rectangle;
		public function ColumnaSnap(ancho:int,alto:int) {
			// constructor code
			hasPast=false;
			dimensiones=new Rectangle(0,0,ancho,alto);
			column = new Sprite();
			addChild(column);
			
			column.graphics.beginFill(0xE9E9E9);
			column.graphics.drawRect(-ancho/2,0,ancho,alto);
			column.graphics.endFill();
			//this.alpha=.5;
			//crear texto
		}
		public function concluida():void{
			hasPast=true;
			column.graphics.clear();
			column.graphics.beginFill(0xDADADA);
			column.graphics.drawRect(-dimensiones.width/2,0,dimensiones.width,dimensiones.height);
			column.graphics.endFill();
			
		}

	}
	
}
