﻿package com.neoris.view  {
	
	/**
	
	clase que crea el layout tonto para el chart, esta clase no cuenta con ninguna logica
	
	**/
	import com.neoris.componentes.Barra;
	import com.neoris.componentes.ColumnaSnap;
	import com.neoris.componentes.Piso;
	import com.neoris.componentes.viewPiso;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class InicializadorVisual {
		
		private var uCanvas:LoadsPerHourChart;
		public function InicializadorVisual(canvas:LoadsPerHourChart) {
			//crea las columnas para cada hora
			uCanvas=canvas;
			
			uCanvas.listLayout = new Array();
			uCanvas.listCuadritos = new Array();
			
			for(var w:int=0;w<24;w++){
				var columna:ColumnaSnap=new ColumnaSnap(uCanvas.anchoColumna,uCanvas.altoColumna);
				uCanvas.addChild(columna);
				uCanvas.ColumnasCompletas.push(columna);
				columna.x=((uCanvas.anchoColumna+5)*w)+uCanvas.anchoColumna;
				//columna.id="layout"+w;
				columna.name="layout"+w;
				columna.y=50;
				var texto:TextField=new TextField();
				texto.text=w+"";
				uCanvas.addChild(texto);
				texto.x=columna.x-columna.width/2;
				texto.y=columna.y+40;
				var tf:TextFormat=new TextFormat("Arial",9,0x000000,true);
				texto.setTextFormat(tf);
				///////////////////////////////////////////////////////////////////////////				
				var target:DisplayObject = uCanvas.getChildByName(columna.name);
				uCanvas.listLayout.push([uCanvas.getChildIndex(target), columna, w]);
				
				var obj:Object = new Object()
					obj.id = w;
					obj.list= new Array();
				uCanvas.listCuadritos.push(obj)
				/////////////////////////////////////////////////////////////////////////////
			}
			for(var wi:int=0;wi<15;wi++){
				var texto1:TextField=new TextField();
				texto1.text=String(wi);
				texto1.name="cargas_"+wi
				uCanvas.addChild(texto1);
				texto1.x=0;
				texto1.y=370-(wi*uCanvas.altoCubo);
				var tf1:TextFormat=new TextFormat("Arial",9,0x000000,true);
				texto1.setTextFormat(tf1);
			}
			var pisoV:viewPiso=new viewPiso();
			pisoV.name="pisoV";			
			uCanvas.addChild(pisoV);
			pisoV.x=5;
			pisoV.y=375;
			
			var piso:Piso=new Piso();
			piso.name="piso";			
			uCanvas.addChild(piso);
			piso.x=200;
			piso.y=315;
			piso.alpha = .0;

			
			/////////////////////////////////////////////////////////////////////////////////////////////
			var targetPiso:DisplayObject = uCanvas.getChildByName(uCanvas.getChildByName("piso").name);
			for (var i:int=0; i<uCanvas.listCuadritos.length; i++){
				uCanvas.listCuadritos[i].list.push([uCanvas.getChildIndex(targetPiso), uCanvas.getChildByName("piso")]);
			}		
			/////////////////////////////////////////////////////////////////////////////////////////////
		}
	}
}
