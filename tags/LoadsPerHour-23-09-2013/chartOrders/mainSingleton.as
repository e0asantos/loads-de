﻿package {
	import com.neoris.componentes.CuadroGrafico;

	public class mainSingleton{
	    public var origenItem:CuadroGrafico ;
	
		static private var _instance:mainSingleton = null;	
		static public function getInstance():mainSingleton{
			if(_instance == null){
				_instance = new mainSingleton();
			}
			return _instance;
		}
	}
}