﻿package com.neoris.mocks {
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.MouseEvent;
	import com.asblabs.primitives.FisixObject;
	import fl.controls.Button;
	import com.neoris.componentes.ICuadroGrafico;
	
	public class ViewMock {
		
		private var graficoRef:LoadsPerHourChart;
		
		private var tempo:Timer;
		
		public function ViewMock(grafico:LoadsPerHourChart) {
			
			// mock para crear la vista y los cubitos
			graficoRef=grafico;
			//horarios
			//graficoRef.sapHoraLocal("08:55:23");
			/*
			*crear los cubitos mock
			*/
			var cars:Array=[];
			var mcap:Array=[];
			for(var j:int=0;j<graficoRef.ColumnasCompletas.length;j++){
				var cuadro:ICuadroGrafico=graficoRef.crearColumna('','', graficoRef.random(4,4),j,graficoRef.random(0,10),1,0,graficoRef.random(0,120),graficoRef.random(1000,6000000).toString(),"Columna1:\nColumna2:\n","Algo que va aquí\nOtra cosa acá");
				//cuadro.drawPattern();
				cars.push(0);
				mcap.push(0);
			}
			
			graficoRef.lineaCapacidadNew=mcap;
			//graficoRef.lineaVehiculosNew=cars;
			//graficoRef.vehiculosDisponibles(cars);
			graficoRef.capacidadMaxima(mcap);
			/*tempo=new Timer(5000);
			tempo.addEventListener(TimerEvent.TIMER,cambioLineas);
			tempo.start();*/
			graficoRef.maximumPlantCapacity(6);
			
			//boton
			var btn:Button=new Button();
			btn.name="unbtn";
			graficoRef.addChild(btn);
			btn.x=30;
			btn.y=20;
			btn.addEventListener(MouseEvent.CLICK,clik);
			
			//setear la hora
			
		}
		private function clik(evt:MouseEvent):void{
			graficoRef.limpiar();
		}
		private function cambioLineas(evt:TimerEvent):void{
			var cars:Array=[];
			var mcap:Array=[];
			for(var j:int=0;j<graficoRef.ColumnasCompletas.length;j++){
				cars.push(graficoRef.random(2,5));
				mcap.push(graficoRef.random(5,7));
			}
			graficoRef.vehiculosDisponiblesAnim(cars);
			graficoRef.capacidadMaximaAnim(mcap);
		}
		private function dummyClick(evt:MouseEvent):void{
			(graficoRef["piso"] as FisixObject).estatico=false;
		}
	}
	
}
