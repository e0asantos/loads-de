﻿package com.neoris.chart{
	import com.neoris.componentes.ICuadroGrafico;
	
	public interface ILoadsPerHourChart {

		// Interface methods:
		
		/*
		Colores se componen de la siguiente manera
		
		
		Los colores tambien representan el status del pedido.
		
		
		
		0-completed
		1-cancelled
		2-not started
		3-to be confirmed
		4-completed
		5-non-optimal assignation
		6-to jobsite
		7-on jobsite
		8-unloading
		9-simulated
		10-loading
		11-system proposal
		12-line Max plant capacity
		13-overbooking level
		14-vehicle availability
		*/
		function coloresStatus(completed:uint=0x54FF54,cancelled:uint=0xff0000,notStarted:uint=0x00ffff,toBeConfirmed:uint=0xffff00,completedWithDelay:uint=0xff6600,nonOptimalAssignation:uint=0x666666,toJobsite:uint=0x336600,onJobsite:uint=0x339900,unLoading:uint=0x33cc00,simulated:uint=0x00d1ff,loading:uint=0x999999,systemProposal:uint=0x663399,lineMaxPlantCapacity:uint=0x66ccff,overbookingLevel:uint=0xcc6600,vehicleAvailability:uint=0XCC00FF):void;
		
		/*
		inicia las configuraciones de la columna que realiza el snap
		y de los cuadritos individuales
		*/
		function inicializarConfiguraciones(wColumna:int=18,hColumna:int=275,wCubo:int=18,hCubo:int=25):void;
		
		/*
		inserta elementos en forma de columna, puede ingresarse desde 1, el tipo
		de pedido es determinado por los números de los colores, la altura de la caída
		no es importante
		*/
		function crearColumna(planta:String, plantaOptima:String,elementos:int,columna:int,tipoPedido:int,numeroPedido:Number,minutos:int,posicion:Number,color:String="0xcccccc",columna1ToolTip:String="",columna2ToolTip:String="",alturaCaida:Number=-30,isFixed:Boolean=false, titleToolTip:String='Order'):ICuadroGrafico;
		
		/*
		Obtiene una descripción de todos los pedidos de una columna
		En el siguiente formato
		
		Array[
		
		object:{
			tipo:int,
			pedido:int,
		}
		
		]
		
		*/
		function getColumna(hora:int):Array;
		
		/*
		*linea que determinan la capacidad maxima
		*se inyectan en forma de array utilizando el numero maximo
		*ej: [4,6,7,2]<-- numeros de camiones en la capacidad máxima
		*
		*/
		
		function capacidadMaximaAnim(capacidad:Array):void;
		
		/*
		*linea que determinan los vehiculos disponibles
		*se inyectan en forma de array utilizando el numero de vehiculos
		*ej: [4,6,7,2]<-- numeros de camiones
		*
		*/
		
		function vehiculosDisponiblesAnim(vehiculos:Array):void;
		
		/*
		*
		*
		*
		*/
		function limpiar():void;
		
		/*
		*Máxima capacidad de la planta
		*valor estatico
		*/
		function maximumPlantCapacity(valor:int):void;
		
		/*
		*
		*Setea la hora local
		*/
		function sapHoraLocal(hora:String):void;
		
		/*
		*
		*borra todos los cuadros que tengan numeroPedido igual al definido
		*/
		function borrarPorPedido(numeroPedido:int):void;
		
		/*
		*
		*borra todos los cuadros que tengan posicion igual al definido
		*/
		function borrarPorPosicion(posicion:int):void;
		
		function capacidadMaximaDeCargas(cargas:int):void;
		
		/*
		*
		*borra el arreglo que hace referencia a la lista de elementos, cuadrosGraficos
		*/
		function cleanReferenceItem():void;
		/*
		*
		*se especifica si muestra o no la grafica al momento de recibir datos
		*/
		function showMe(view:Boolean):void;
		
		function getOrigenItem():Object;
		
			
		function colocaVehiculosDispLine(vehicleDispArray:Array):void;
		
	}
	
	
	
}
