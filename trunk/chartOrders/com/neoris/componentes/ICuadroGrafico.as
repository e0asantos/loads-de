﻿package com.neoris.componentes {
	
	public interface ICuadroGrafico {

		// Interface methods:
		function set horario(value:int):void;
		
		function get horario():int;
		
		function highLight():void;
		
		function drawPattern(activar:Boolean=true):void;
		
		function paintBorderCuadro():void;
	}
	
}
