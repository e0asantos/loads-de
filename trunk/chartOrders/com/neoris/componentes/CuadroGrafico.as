﻿package com.neoris.componentes
{


	import com.neoris.componentes.Barra;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BlurFilter;
	import flash.filters.GlowFilter;
	import flash.utils.Dictionary;

	/*import flash.display.GraphicsGradientFill;
	import flash.display.IGraphicsData;
	import flash.geom.Matrix;
	import flash.display.GraphicsStroke;
	import flash.display.GraphicsPath;
	import flash.display.JointStyle;
	import flash.display.GraphicsSolidFill;*/


	public class CuadroGrafico extends Barra implements ICuadroGrafico
	{
		private var square:Sprite;
		
		public var numeroPedido:Number;
		public var apuntadorPedidos:Dictionary;
		public var minutos:int;
		public var _horario:int;
		public var horarioPrev:int;
		public var horarioInRange:int;
		public var horarioOriginal:int;
		public var simulado:Boolean;
		public var bloquear:Boolean;
		public var thisColor:uint;
		public var colorBorder:uint;
		public var posicion:Number;
		private var _cambio:Boolean;
		public var colorStr:String;
		public var horarioStatico:int;
		public var flagHorarioStatico:Boolean = false;
		public var plantaOptima:String = '';
		public var plantaActual:String = '';

		public var tituloTooltip:String;
		public var col1:String;
		public var col2:String;

		public var rayas:Sprite = new Sprite();

		private var __alto:int;
		private var __ancho:int;
	
		public var tipo:int = 0;
		public var isFix:Boolean;
	
		
		
		
		public function CuadroGrafico(ancho:int,alto:int,color:String, colorBorder:uint=0xffffff){
			super();
			isFix = false;
			__alto = alto;
			__ancho = ancho;
			_cambio = false;
			this.doubleClickEnabled = true;
			bloquear = false;
			simulado = false;
			colorStr = color;
			
			paint()
			
			this.addEventListener(MouseEvent.MOUSE_OVER,ratonEncima);
			this.addEventListener(MouseEvent.MOUSE_OUT,ratonFuera);
		}
		
		public function paintBorderCuadro():void{
			paint(0xFF0000)
		}
		
		
		private function paint(border:uint=0xffffff):void{
			/**/
			var colors:Array = colorStr.split('/');
			if (String(colors[0]).indexOf("-") != -1){
				thisColor = uint(colors[0].substr(0,-2));
			}else{
				thisColor = uint(colors[0]);
			}/**/
			this.graphics.clear();
			this.rayas.graphics.clear();
			//if(border != 0xffffff){
				//this.graphics.lineStyle(2,border);
			//}
			
			this.graphics.beginFill(thisColor);
			this.graphics.drawRoundRect(0,0,__ancho,__alto,10);
			this.alpha = 1;
			this.graphics.endFill();			
			
			this.rayas.graphics.clear();
			this.rayas.graphics.lineStyle(2,0x000000);
			this.rayas.graphics.moveTo(__ancho,0);
			this.rayas.graphics.lineTo(0,__alto);
			this.addChild(rayas);
			
			this.cacheAsBitmap = true;
		}
		
		public function set horario(value:int):void{
			if(flagHorarioStatico == false){
				horarioStatico = value;
				flagHorarioStatico = true;
			}
			
			if (! _cambio){
				horarioOriginal = value;
			}
			_horario = value;
		}

		public function get horario():int{
			return _horario;
		}
		private function ratonEncima(evt:MouseEvent):void{
			for (var q in this.apuntadorPedidos[numeroPedido]){
				this.apuntadorPedidos[numeroPedido][q].highLight();
			}
		}
		private function ratonFuera(evt:MouseEvent):void{
			for (var q in this.apuntadorPedidos[numeroPedido]){
				//trace(this.apuntadorPedidos[numeroPedido][q]);
				//this.apuntadorPedidos[numeroPedido][q].highLight();
				this.apuntadorPedidos[numeroPedido][q].quitar();
			}
		}
		public function highLight():void{
			quitar();
			var filtros:Array = this.filters;
			filtros.push(new GlowFilter(0x123456,1,10,10,4));
			this.filters = filtros;
		}
		
		private function quitar():void{
			var filtros:Array = this.filters;
			for (var w:int=filtros.length; w>-1; w--)
			{
				if (filtros[w] is flash.filters.GlowFilter)
				{
					filtros.splice(w-1,1);
				}
			}
			this.filters = filtros;
		}
		
		
		
		public function drawPattern(activar:Boolean=true):void{
			this.rayas.visible = activar;			
		}

	}

}