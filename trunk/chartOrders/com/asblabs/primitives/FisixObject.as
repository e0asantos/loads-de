﻿package  com.asblabs.primitives{
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.filters.BlurFilter;
	import com.asblabs.primitives.CartesianObject;
	
	public class FisixObject extends CartesianObject {
		
		
		
		/*
			primitive properties
		*/
		private var _masa:Number;
		private var _aceleracion:Number;
		private var _velocidad:Number;
		public var _estatico:Boolean;
		private var _lastEstatico:Boolean;
		private var _pasivo:Boolean=false;
		
		public function FisixObject() {
			this.addEventListener(Event.ADDED_TO_STAGE,init);			
		}
		protected function init(evt:Event):void{
			this._masa=random(1,5);
			_estatico=false;
		}
		
		public function set masa(valor:Number):void{
			_masa=masa;
		}
		
		public function get masa():Number{
			return _masa;
		}
		override public function stopDrag():void{
			super.stopDrag();
			this.estatico=this._lastEstatico;
		}
		override public function startDrag(lockCenter:Boolean=false,bounds:Rectangle=null):void{
			super.startDrag(lockCenter,bounds);
			this._lastEstatico=this.estatico;
			this.estatico=true;
		}
		public function random(min:Number,max:Number=NaN):int {
			if (isNaN(max)) { max = min; min=0; }
			// Need to use floor instead of bit shift to work properly with negative values:
			return Math.floor(float(min,max));
		}
		public function float(min:Number,max:Number=NaN):Number {
			if (isNaN(max)) { max = min; min=0; }
			return Math.random()*(max-min)+min;
		}
		
		public function set estatico(valor:Boolean):void{
			this._estatico=valor;
		}
		public function get estatico():Boolean{
			return this._estatico;
		}
		public function get pasivo():Boolean{
			return this._pasivo;
		}
		public function set pasivo(valor:Boolean):void{
			this._pasivo=valor;
		}
	}
	
}
