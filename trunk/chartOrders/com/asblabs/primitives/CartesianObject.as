﻿package com.asblabs.primitives{
	import flash.display.Sprite;
	import flash.events.Event;
	import com.asblabs.juegoUtil;
	import flash.geom.Point;
	
	public class CartesianObject extends Sprite{
		
		/*
			coordinates
		*/
		private var originX:Number = 0;
        private var originY:Number = 0;
		
		private var lastX:Number=0;
		private var lastY:Number=0;
		
		public function CartesianObject() {
			// constructor code
			this.addEventListener(Event.ADDED_TO_STAGE,startVars);
			
		}
		private function startVars(evt:Event):void{
			originX=0;
			if(juegoUtil.CENTRO!=null){
				
			}
			originY=50;
		}
		public function set origen(punto:Point):void{
			/*originX=punto.x;
			originY=punto.y;*/
		}
		public function get origin():Point{
			return new Point(originX,originY);
		}
		override public function set x($x:Number):void {
			/*if(juegoUtil.CENTRO!=null){
				originX=juegoUtil.CENTRO.x;
			}*/
			this.lastX=super.x;
            super.x = originX + $x; // use super to avoid these setters and getters
        }
        override public function set y($y:Number):void {
			/*if(juegoUtil.CENTRO!=null){
				originY=juegoUtil.CENTRO.y;
			}*/
			super.y = originY + $y;
			this.lastY=super.y;
        }

        override public function get x():Number {
            return super.x - originX;
        }

        override public function get y():Number {
            return super.y - originY;
        }
	}	
}
