﻿package asblabs{
	 import flash.display.Loader;
    import flash.display.Sprite;
    import flash.events.*;
    import flash.net.URLRequest;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.events.NetStatusEvent;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;		
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.gskinner.motion.*;
	import com.gskinner.motion.plugins.*;
	import com.gskinner.motion.easing.*;
	import nl.hdi.math.pathfinding.AStar;
	import nl.hdi.math.pathfinding.PathNode;
	import nl.hdi.math.pathfinding.Node;
	import asblabs.HitTest;
	import asblabs.gameUtils;
	import asblabs.Events.gameUtilEvent;
	import flash.net.LocalConnection;
	
	import caurina.transitions.properties.ColorShortcuts;
	
	public class mainActivity extends Sprite{
		public var video:Video;
		public var nc:NetConnection;
		public var ns:NetStream;
		public var pop:popUp;
		public var escenario:Escenario;
		public var instrucciones:panelInstrucciones;
		public var guyBtn:ChicoBoton;
		public var db:dummyBtn;
		public var personaje:Matias;
		public var patron:Patron;
		public var mloader:Loader;
		public var peticion:URLRequest;
		public var conn:LocalConnection;
		//animaciones
		protected var timeline:TimelineLite;
		//pathfinder
		private var nodes : Array = [];
		private var rows : int = 31;//61
		private var columns : int = 53;//103
		private var spacing : Number = 20;
		
		private var astar : AStar = new AStar();
		private var start : Node;
		private var target : Node;
		private var recorrido:Array;
		private var lastNode:Node;
		private var lastNoded:Node;
		private var lugar1:Lugar1;
		private var ultimoLugar:String;
		private var videoExito:Boolean;
		private var lastMatias:Object;
		
		//lugares
		private var lugar11:Lugar11;
		private var lugar22:Lugar22;
		private var lugar33:Lugar33;
		private var lugar44:Lugar44;
		private var lugar55:Lugar55;
		private var lugar66:Lugar66;
		
		
		public function mainActivity():void{
			super();
			videoExito=false;
			lastMatias=new Object();
			lastMatias.x=0;
			lastMatias.y=0;
			conn=new LocalConnection();
			conn.connect("juegos");
			conn.client=this;
			timeline = new TimelineLite({onComplete:function(){
										try{
											personaje.gotoAndStop(1);
											////////LUGARES/////////////////////
											if(HitTest.complexHitTestObject(personaje,lugar11)){
												cargadorJuegos("game1.swf");
												ultimoLugar="game1";
											} else if(HitTest.complexHitTestObject(personaje,lugar22)){
												cargadorJuegos("game2.swf");
												ultimoLugar="game2";
											}//......etc etc etc
											
											
											
											
											/////////////////////////////////////
										} catch(e){
											
										}
										}});
			trace("inicia");
			trace("play FLV");
			
			ColorShortcuts.init();
			MotionBlurPlugin.install();
			MotionBlurPlugin.strength = 2;
			
			video=new Video(1024,686);
			nc=new NetConnection();
			nc.connect(null);
			ns=new NetStream(nc);
			ns.bufferTime=10;
			ns.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			video.attachNetStream(ns);
			var url:String="http://www.jamex.com.mx/juegomontenegro/IntroMonteMundo.flv";
			ns.play(url);
			trace(url);
			addChild(video);
			showPopUp("Cargando video....");
			
			//cuando se quiten los comentarios quitar la siguiente linea tmb
			//initInstructions();
			
		}
		public function finito(msg:String){
			if(msg=="restart"){
				mloader.unload();
			} else {
				mloader.unload();
				//reproducir los videos de exito
				if(ultimoLugar=="game1"){
					playVideo("http://www.jamex.com.mx/juegomontenegro/IntroMonteMundo.flv");
				} else if(ultimoLugar=="game2"){
					//--------------------------------->
					//playVideo("http://www.jamex.com.mx/juegomontenegro/IntroMonteMundo2.flv");
					//cargadorJuegos("game2.swf");
					//escenario.gotoAndStop(1);
					
					
					
					
					
					//<----------------------------------------
				} else if(ultimoLugar=="game3"){
					
				}//.........etc etc etc
			}
		}
		protected function showPopUp(msg:String):void{
			if(pop==null){
			pop=new popUp();
			addChild(pop);
		}
			pop["message"].text=msg;
			center(pop,true);
		}
		protected function hidePopUp():void{
			try{
				removeChild(pop);
				pop==null;
			}catch(e){
				
			}
		}
		protected function netStatusHandler(evt:NetStatusEvent):void{
			//trace(evt.info.code);
			if(evt.info.code=="NetStream.Play.Stop"){
				trace("==Pasar a instrucciones==");
				if(!videoExito){
				initInstructions();
				}
				removeChild(video);
				videoExito=false;
				video=null;
			} else if(evt.info.code=="NetStream.Buffer.Full"){

				hidePopUp();
			}
		}
		protected function initInstructions():void{
			escenario=new Escenario();
			instrucciones=new panelInstrucciones();
			guyBtn=new ChicoBoton();
			//lugar2=new Lugar2();
			addChild(escenario);
			//addChild(lugar1);
			
			lugar11=new Lugar11();
			addChild(lugar11);
			lugar22=new Lugar22();
			addChild(lugar22);
			lugar33=new Lugar33();
			addChild(lugar33);
			lugar44=new Lugar44();
			addChild(lugar44);
			lugar55=new Lugar55();
			addChild(lugar55);
			lugar66=new Lugar66();
			addChild(lugar66);
			
			addChild(instrucciones);
			addChild(guyBtn);
			center(escenario);
			center(instrucciones,false,70,80);
			center(guyBtn,true,210,-20);
			guyBtn.addEventListener(MouseEvent.CLICK,initGame);
			guyBtn.addEventListener(MouseEvent.MOUSE_OUT,botonOut);
			guyBtn.addEventListener(MouseEvent.MOUSE_OVER,botonOver);
		}
		protected function botonOut(evt:MouseEvent):void{
			guyBtn.gotoAndStop(1);
		}
		protected function botonOver(evt:MouseEvent):void{
			guyBtn.gotoAndStop(2);
		}
		protected function center(elemento:Sprite,esquina:Boolean=false,ancho:Number=0,alto:Number=0){
			if(!esquina){
				elemento.x=elemento.width/2+ancho;
				elemento.y=elemento.height/2+alto;
			} else {
				elemento.x=stage.width/2+ancho;
				elemento.y=stage.height/2+alto;
			}
			
		}
		protected function initGame(evt:MouseEvent):void{
			trace("==iniciando juego==");
			if(personaje==null){
			/*GTweener.to(guyBtn, .5, {alpha:0});
			GTweener.to(instrucciones, .5, {alpha:0});*/
			removeChild(guyBtn);
			removeChild(instrucciones);
			//hacer caer al monito
			
			personaje=new Matias();
			addChild(personaje);
			personaje.addEventListener(Event.ENTER_FRAME,posicionador);
			personaje.x=stage.width/2;
			personaje.y=-30;
			personaje.width=20;
			personaje.height=40;
			GTweener.to(personaje, .4, {y:stage.height/2,delay:.5});
			lastMatias.x=personaje.x;
			lastMatias.y=personaje.y;
			//ver patron
			patron=new Patron();
			addChild(patron);
			patron.alpha=.1;
			makeGrid();
			}
		}
		
		//nodos para el reconocimiento de path
		protected function makeGrid():void{
			// Generate nodes
			var total : int = rows * columns;
			for (var i : Number = 0; i < total; i++) {
				var node : Node = new Node();
				
				node.x =  ( spacing * int(i%columns) );
				node.y =  ( spacing * int(i/columns) );
				
				nodes.push(node);
				
				addChild(node);
				node.content.alpha=0;
				
				node.buttonMode = true;
				node.addEventListener(MouseEvent.CLICK, onClick);
			}
			connect();
			generateNewField();
		}
		private function resetField() : void {
			target = null;
			start = null;
			for (var i : Number = 0; i < nodes.length; i++) {
				if( (nodes[i] as Node).node.valid ) {
					(nodes[i] as Node).setDefault();
				} else {
					(nodes[i] as Node).setBlock();
				}
			}
		}
		
		/**
		 * Invoked when the user clicks on a Node
		 */
		private function onClick( event : MouseEvent ) : void {
			
			//buscar el nodo mas cercano y visible en el q matias esta
			var s:int=0;
			while(s<nodes.length){
				if(personaje.hitTestObject(nodes[s]) && nodes[s].visible){
					start=nodes[s];
					break;
				}
				s++;
			}
			target=null;
			if(!start) {
				
				start = (event.currentTarget) as Node;
				start.setStart();
			
			// Set target	
			} else if(!target) {
				target = (event.currentTarget) as Node;
				target.setStart();
				target.setTarget();
				
				var startTime : int = getTimer();
				
				var path : Array = astar.getPath(start.node, target.node);
				recorrido=path;
				//trace("mypath:"+path);
				/*
				MATIAS CAMINA
				*/

				//agregar posiciones
				var w:int=0;
				timeline.stop();
				timeline.clear();
				while(w<path.length){
					timeline.append( new TweenLite(personaje, .4, {x:path[w].x,y:path[w].y,ease:Linear.easeNone}));
					w++;
				}
				personaje.gotoAndStop(2);
				timeline.gotoAndPlay(0)
				var duration : int = getTimer() - startTime;
				
				// Update status
				if(path.length == 0) {
					trace("No path found");
				} else {
					trace( "Path found in " + duration + " ms");
				}
				
				// Update path graphics			
				for (var i : Number = 0; i < path.length; i++) {
					if(path[i].data != start && path[i].data != target) {
						(path[i].data as Node).setPath();
					}
				}
			
			// Reset field
			} else {			
				resetField();
			}
		}
		private function generateNewField() : void {
			target = null;
			start = null;
			
			for (var i : Number = 0; i < nodes.length; i++) {
				if(!HitTest.complexHitTestObject((nodes[i] as Node),patron)){
					(nodes[i] as Node).node.valid = false;
					(nodes[i] as Node).setBlock();
					(nodes[i] as Node).visible=false;
					(nodes[i] as Node).mouseChildren = false;
					(nodes[i] as Node).mouseEnabled = false;
				} else {
					//(nodes[i] as Node).visible=false;
				}
			}
		}
		private function connect() : void {
			graphics.lineStyle(1, 0x0, 0.2);
			for (var j : Number = 0; j < nodes.length; j++) {
				
				var node : Node = nodes[j];
				
				var up : int = j - columns;
				var down : int = j + columns;
				var left : int = j - 1;
				var right : int = j + 1;
				
				var node2 : Node;
				
				if(up >= 0) {
					node2 = nodes[up] as Node;
					node.node.addNode( node2.node ); 
					graphics.moveTo(node.x, node.y);
					graphics.lineTo(node2.x, node2.y);
				}
				
				if(down < nodes.length) {
					node2 = nodes[down] as Node;
					node.node.addNode( node2.node );
					graphics.moveTo(node.x, node.y);
					graphics.lineTo(node2.x, node2.y);
				}
				
				if(j % columns != 0) {
					node2 = nodes[left] as Node;
					node.node.addNode( node2.node );
					graphics.moveTo(node.x, node.y);
					graphics.lineTo(node2.x, node2.y);
				}
				
				if(right % columns != 0) {
					node2 = nodes[right] as Node;
					node.node.addNode( node2.node );
					graphics.moveTo(node.x, node.y);
					graphics.lineTo(node2.x, node2.y);
				}
			}
		}
		public function cargadorJuegos(juegoURL:String):void{
			mloader=new Loader();
			peticion = new URLRequest(juegoURL);
			configureListeners(mloader.contentLoaderInfo);
            mloader.load(peticion);
		}
		private function configureListeners(dispatcher:IEventDispatcher):void {
            dispatcher.addEventListener(Event.COMPLETE, completeHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(Event.UNLOAD, unLoadHandler);
        }
		 private function completeHandler(event:Event):void {
            //trace("completeHandler: " + event);
			            addChild(mloader);
						hidePopUp();
        }
		private function progressHandler(event:ProgressEvent):void {
            //trace("progressHandler: bytesLoaded=" + event.bytesLoaded + " bytesTotal=" + event.bytesTotal);
			showPopUp("Cargado "+Math.ceil((event.bytesLoaded*100)/event.bytesTotal)+" %");
        }

        private function unLoadHandler(event:Event):void {
            //trace("unLoadHandler: " + event);
        }
		private function playVideo(videoURL:String):void{
			videoExito=true;
			video=new Video(1024,600);
			nc=new NetConnection();
			nc.connect(null);
			ns=new NetStream(nc);
			ns.bufferTime=10;
			ns.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			video.attachNetStream(ns);
			var url:String=videoURL;
			ns.play(url);
			trace(url);
			addChild(video);
			showPopUp("Cargando video....");
		}
		private function posicionador(evt:Event):void{
			if(lastMatias.x==personaje.x && lastMatias.y==personaje.y){
				//personaje.gotoAndStop(1);
				lastMatias.x=personaje.x;
				lastMatias.y=personaje.y;
			} else if(personaje.x>lastMatias.x && personaje.y>lastMatias.y){
				personaje.gotoAndStop(2);
				lastMatias.x=personaje.x;
				lastMatias.y=personaje.y;
			} else if(personaje.x>lastMatias.x && personaje.y<lastMatias.y){
				personaje.gotoAndStop(4);
				lastMatias.x=personaje.x;
				lastMatias.y=personaje.y;
			} else if(personaje.x<lastMatias.x && personaje.y<lastMatias.y){
				personaje.gotoAndStop(8);
				lastMatias.x=personaje.x;
				lastMatias.y=personaje.y;
			} else if(personaje.x<lastMatias.x && personaje.y<lastMatias.y){
				personaje.gotoAndStop(10);
				lastMatias.x=personaje.x;
				lastMatias.y=personaje.y;
			} else if(personaje.x<lastMatias.x && personaje.y>lastMatias.y){
				personaje.gotoAndStop(8);
				lastMatias.x=personaje.x;
				lastMatias.y=personaje.y;
			}
		}
	}
	
	
	
}