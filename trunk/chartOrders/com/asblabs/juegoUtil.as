﻿package com.asblabs{
	
	import com.asblabs.events.*;
	import com.asblabs.layouts.*;
	import com.asblabs.primitives.*;
	import com.asblabs.utils.*;
	import com.coreyoneil.collision.CollisionList;
	import com.greensock.*;
	import com.greensock.core.*;
	import com.greensock.easing.*;
	
	import fl.transitions.Tween;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filters.BlurFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.LocalConnection;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.setTimeout;
	import com.neoris.componentes.CuadroGrafico;
	
	
	[Event(name="collission",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="actionkey",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="validhit",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="invalidhit",type="com.asblabs.events.juegoUtilEvent")]

	[Event(name="maxscore",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="minscore",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="timeout",type="com.asblabs.events.juegoUtilEvent")]

	[Event(name="second",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="complexhit",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="click2",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="flashSingleCycleOperation",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="flashSingleCycleOperation",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="startDrag",type="com.asblabs.events.juegoUtilEvent")]
	
	[Event(name="stopDrag",type="com.asblabs.events.juegoUtilEvent")]
	
	public class juegoUtil extends Sprite{
		
		private var dragger:Object;
		private var lastClickedObject:DisplayObject;
		private var cursores:Array;
		private var cursoresPermisos:Array;
		public var dropAreas:Object;
		public var actionKeys:Array;
		public var invalidosHits:Array;
		public var validosHits:Array;
		public var todosHits:Array;
		public var dateQuery:Date;
		
		private var scores:Object;
		private var globalScore:Number;
		private var globalTime:Number;
		private var timer:Timer;
		private var complexHips:Array;
		public var carrousels:Array;
		private var mouseLocker:Object;
		private var pauseCarrousel:Boolean;
		private var lastCarrouselPosition:Object;
		private var listaGrids:Object;
		private var erratics:Object;
		private var returnToOriginal:Object;
		private var conn:LocalConnection;
		
		//fisica
		private var _displaysTotales:int;
		private var _dummyCounter:int;
		private var _dummyCounter2:int;
		private var boundingBox:Sprite;
		private var boundingBoxProps:Array;
		private var computeRect:Rectangle;
		private var filtros:Array;
		
		private var FisixComputations:Dictionary;
		
		/*
		setea el centro del escenario, si no se especifica
		toma el de la esquina inferior izquierda
		*/
		public static var CENTRO:Point;
		
		private var _ignoredClass:Array;
		private var _ignoredNames:Array;
		
		private var dropShadow:BlurFilter=new BlurFilter(6,6);
		
		public var piso:Object
		public var listLayout:Array = new Array();
		public var listCuadritos:Array = new Array();
		
		
		/**
		* constantes que controlan el comportamiento de fisica
		*
		*
		**/
		
		public var GRAVITY		:Number = 9.8;
		public var FRICTION		:Number = .9;
		public var IMMOVABLE		:Number = 100000;
		
		public function juegoUtil():void{
			addEventListener(Event.ADDED_TO_STAGE, stageInit);
		}
		private function stageInit(event:Event):void{
			//fisixField=new CollisionList(unabarra);
			CENTRO=new Point(0,stage.stageHeight);
			FisixComputations=new Dictionary();
			conn=new LocalConnection();
			erratics=new Object();
			returnToOriginal=new Object();
			listaGrids=new Object();
			lastCarrouselPosition=new Object();
			lastCarrouselPosition.x=0;
			lastCarrouselPosition.y=0;
			pauseCarrousel=false;
			carrousels=[];
			complexHips=[];
			globalScore=0;
			globalTime=0;
			scores=new Object();
			dragger=new Object();
			actionKeys=[];
			cursores=[];
			dropAreas=new Object();
			cursoresPermisos=[];
			invalidosHits=[];
			validosHits=[];
			todosHits=[];
			ScoreLimite();
			_ignoredClass=[];
			_ignoredNames=[];
			mouseLocker=new Object();			
			filtros=[];
			
			stage.addEventListener(MouseEvent.CLICK,identificarObjeto);
			stage.addEventListener(KeyboardEvent.KEY_DOWN,teclaAbajo);
			addEventListener(Event.ENTER_FRAME,seeHit);
			
			//sirve para agregar nuevos objetos añadidos

			stage.addEventListener(Event.ADDED,objetoCreado);
			stage.addEventListener(Event.REMOVED,objetoDestruido);
			
		}
		
		
		
		public function comportamientoErratico(UI:DisplayObject,Xdistance:int=10,Ydistance:int=10,repeat:Boolean=false,xmin:int=0,xmax:int=1000,ymin:int=0,ymax:int=1000):void{
			erratics[UI.name]={x:Xdistance,y:Ydistance,repeat:repeat,xmin:xmin,xmax:xmax,ymin:ymin,ymax:ymax};
			//iniciar animacion
			
			TweenLite.to(UI, 1, {x:UI.x+random(-1*Xdistance,Xdistance),y:UI.y+random(-1*Ydistance,Ydistance),onComplete:function(){
						erraticRepeater(UI);
						 }}); 
		}
		private function erraticRepeater(UI:DisplayObject):void{
			 if(erratics[UI.name].repeat){
				 var myx:Number=UI.x+random(-1*erratics[UI.name].x,erratics[UI.name].x);
				 var myy:Number=UI.y+random(-1*erratics[UI.name].y,erratics[UI.name].y);
				 if(myx>erratics[UI.name].xmin && myx<erratics[UI.name].xmax && myy>erratics[UI.name].ymin && myy<erratics[UI.name].ymax){
							TweenLite.to(UI, 1, {x:UI.x+random(-1*erratics[UI.name].x,erratics[UI.name].x),y:UI.y+random(-1*erratics[UI.name].y,erratics[UI.name].y),onComplete:function(){
						erraticRepeater(UI);
						 }});  
				 } else {
					 //animar a un lugar a salvo
					 var savex:Number=random(erratics[UI.name].xmin,erratics[UI.name].xmax-(UI.width*2));
					 var savey:Number=random(erratics[UI.name].ymin,erratics[UI.name].ymax-(UI.height*2));
					 TweenLite.to(UI, 1, {x:savex,y:savey,onComplete:function(){
						erraticRepeater(UI);
						 }});
				 }
						 }


else {
							 erratics[UI.name]=null;
						 }
		}
		public function Arrastrable(UI:DisplayObject,DropArea:DisplayObject=null,returnToOrigialPositionAfterDrop:Boolean=false,bringToFront:Boolean=true):void{
			dropAreas[UI.name]=DropArea;
			returnToOriginal[UI.name]={r:returnToOrigialPositionAfterDrop,x:0,y:0,i:bringToFront};
			UI.addEventListener(MouseEvent.MOUSE_DOWN,moverObjeto);
			UI.addEventListener(MouseEvent.MOUSE_UP,soltarObjeto);
			UI.addEventListener(MouseEvent.MOUSE_MOVE,moverObjeto);
		}
		
		public function noArrastrable(UI:DisplayObject):void{
			dropAreas[UI.name]=null;
			returnToOriginal[UI.name]=null;
			//UI.removeEventListener(MouseEvent.MOUSE_DOWN,moverObjeto);
			UI.removeEventListener(MouseEvent.MOUSE_UP,soltarObjeto);
			//UI.removeEventListener(MouseEvent.MOUSE_MOVE,moverObjeto);
			
		}
		public function detenerAnimacionDe(UI:DisplayObject):void{
			var a:Array = TweenMax.getAllTweens();
			var isDC:Boolean; //is delayedCall 
			var i:int = a.length;
			while (--i > -1) {
				if(TweenLite(a[i]).target==UI){
					//TweenLite(a[i]).target;
					TweenCore(a[i]).paused=true;
				}
			}
		}
		public function detectarClicksDe(UI:DisplayObject):void{
			UI.addEventListener(MouseEvent.CLICK,function(evt:MouseEvent){
								dispatchEvent(new juegoUtilEvent("click2",evt.target));
								});
		}
		public function UsarCursoresEn(UI:DisplayObject,selectWithMouse:Boolean=false,xmin:int=0,xmax:int=1000,ymin:int=0,ymax:int=1000,useDegrees:Boolean=false):void{
			UI.addEventListener(MouseEvent.CLICK,identificarObjeto);
			cursores.push(UI);
			cursoresPermisos.push({usemouse:selectWithMouse,xmin:xmin,xmax:xmax,ymin:ymin,ymax:ymax,degrees:useDegrees});
			//objeto{useMouse:true|false}
		}
		public function noUsarCursoresEn(UI:DisplayObject):void{
			UI.removeEventListener(MouseEvent.CLICK,identificarObjeto);
			if(cursores.indexOf(UI)!=-1){
				var indice:int=cursores.indexOf(UI);
				cursores.splice(indice,1);
				cursoresPermisos.splice(indice,1);
			}
		}
		
		//add-detectar,escuchar,
		public function UsarTecla(ASCIICode:int,nombre:String):void{
			actionKeys[ASCIICode]=nombre;
		}
		public function EsGolpeValido(ElementoQueEsperaGolpe:DisplayObject,ElementoQueDaGolpe:DisplayObject):void{
			if(todosHits.indexOf(ElementoQueEsperaGolpe)==-1){
				todosHits.push(ElementoQueEsperaGolpe);
			}
			validosHits.push(ElementoQueDaGolpe);
		}
		public function EsGolpeInvalido(ElementoQueEsperaGolpe:DisplayObject,ElementoQueDaGolpe:DisplayObject):void{
			if(todosHits.indexOf(ElementoQueEsperaGolpe)==-1){
				todosHits.push(ElementoQueEsperaGolpe);
			}
			invalidosHits.push(ElementoQueDaGolpe);
		}
		public function set score(val:Number):void{
			if(val==scores["min"] && scores["min"]!=-10){
				dispatchEvent(new juegoUtilEvent("minscore"));
			} else if(val==scores["max"]){
				dispatchEvent(new juegoUtilEvent("maxscore"));
			}
			globalScore=val;
		}
		public function get score():Number{
			return globalScore;
		}
		
		public function ScoreLimite(min:Number=-10,max:Number=1000):void{
			scores["min"]=min;
			scores["max"]=max;
		}
		public function CuentaRegresiva(val:int=10):void{
			try{
				timer.stop();
				timer.removeEventListener(TimerEvent.TIMER,conteoTiempo);
				timer=null;
			} catch(e){
				
			}
			if(globalTime==0){
				globalTime=val;
			}
			timer.addEventListener(TimerEvent.TIMER,conteoTiempo);

		}
		public function IniciarCuenta():void{
			try{
				timer.start();				
			} catch(e){
				
			}
		}
		public function DetenerCuenta():void{
			try{
				timer.stop();
			} catch(e){
				
			}
		}
		public function get duracion():int{
			return globalTime;
		}
		public function set duracion(val:int):void{
			globalTime=val;
			CuentaRegresiva(globalTime);
			IniciarCuenta();
		}
		public function UsarGolpeComplejo(primero:DisplayObject,segundo:DisplayObject,oneNotificationOnly:Boolean=false):void{
			complexHips.push({p:primero,s:segundo,lock:oneNotificationOnly});
		}
		public function AnimarEnLinea(items:Array,property:String,stepTime:int=1,begin:int=100,end:int=0,pauseWhenClick:Boolean=false):void{
			carrousels.push({elementos:items,propiedad:property,pasoTiempo:stepTime,termina:end,comienzo:begin,whenclick:pauseWhenClick});
			//iniciar carrusel
			for(var q in items){
				if(property=="x"){
					TweenMax.to((items[q] as DisplayObject), (q*stepTime)+stepTime, {x:end,ease:Linear.easeNone});
				} else if(property=="y"){
					TweenMax.to((items[q] as DisplayObject), (q*stepTime)+stepTime, {y:end,ease:Linear.easeNone});
				}
			}
			
		}
		public function difuminar(UI:DisplayObject,tiempo:int=1):void{
			TweenMax.to(UI, tiempo, {alpha:0,onComplete:function(){
						removeChild(UI);
						}});
		}
		public function random(low:Number=0, high:Number=1):Number{
			return Math.floor(Math.random() * (1+high-low)) + low;
		}
		public function comm(accion:String,endMethod:String):void{
			//trace("SEND>");
			conn.send("juegos",endMethod,accion);
		}
		public function compareArray(a1:Array,a2:Array):Boolean{
			var b:Boolean=true;
			if(a1.length!=a2.length) b=false;
			for(var q in a1){
				if(a1[q]!=a2[q]){
					b=false;
					break;
				}
			}
			return b;
		}
		public function CrearMosaico(sprite:Class,nom:String="mygrid",columns:int=1,rows:int=1,space:int=0,values:Array=null,xx:int=0,yy:int=0,addClickListener:Boolean=false,propertyToSendData:String=null,secondProperty:String=null):void{
			var q:int=0;
			var c:int=0;
			listaGrids[nom]=[];
			while(q++<columns){
				var w:int=0;
				while(w++<rows){
					var visual:DisplayObject=new sprite();
					listaGrids[nom].push(visual);
					addChild(visual);
					if(propertyToSendData!=null && values!=null){
						if(secondProperty!=null){
							visual[propertyToSendData][secondProperty]=values[c];
						} else {
							visual[propertyToSendData]=values[c];
						}
					}
					c++;
					if(addClickListener){
						detectarClicksDe(visual);
					}
					visual.x=xx+((q-1)*(visual.width+space));
					visual.y=yy+((w-1)*(visual.height+space));
				}
			}
		}
		public function QuitarMosaico(nom:String="mygrid"):void{
			var items:Array=listaGrids[nom];
			for(var q in items){
				removeChild(items[q]);
			}
			listaGrids[nom]=null;
		}
		
		public function ignorarFisicaDeClase(nombreClase:String):void{
			if(this._ignoredClass.indexOf(nombreClase)==-1){
				this._ignoredClass.push(nombreClase);
			}
		}
		public function noIgnorarFisicaDeClase(nombreClase:String):void{
			if(this._ignoredClass.indexOf(nombreClase)!=-1){
				this._ignoredClass.splice(this._ignoredClass.indexOf(nombreClase),1);
			}
		}
		public function ignorarNombreElemento(nombre:String):void{
			if(this._ignoredNames.indexOf(nombre)==-1){
				this._ignoredNames.push(nombre);
			}
		}
		public function noIgnorarNombreElemento(nombre:String):void{
			if(this._ignoredNames.indexOf(nombre)!=-1){
				this._ignoredNames.splice(this._ignoredClass.indexOf(nombre),1);
			}
		}
		
		private function conteoTiempo(evt:TimerEvent):void{
			globalTime-=1;
			dispatchEvent(new juegoUtilEvent("second"));
			if(globalTime<=0){
				globalTime=0;
				timer.stop();
				timer.reset();
				dispatchEvent(new juegoUtilEvent("timeout"));
			}
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		private function onMoveUI(e:MouseEvent):void{
			dispatchEvent(new juegoUtilEvent("moveItem",e.target));
		}
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		private function moverObjeto(evt:MouseEvent):void{
			
			if(dragger[evt.target.name]==undefined){
				dragger[evt.target.name]=false;
			}
	
			//if(evt.target.tipo!= 1 && evt.target.tipo!=4 && evt.target.tipo!= 10){
				//if(evt.target.isFix == false){
					if(evt.type=="mouseDown" && !dragger[evt.target.name]){
						try{							
							dateQuery = new Date(dateQuery.fullYear, dateQuery.month, dateQuery.date);
							var dateActual:Date = new Date();
							dateActual = new Date(dateActual.fullYear, dateActual.month, dateActual.date);
							
							if(dateQuery >= dateActual){							
								//if(evt.target.tipo!= 1 && evt.target.tipo!=4 && evt.target.tipo!= 6 && evt.target.tipo!=7 && evt.target.tipo!=8 && evt.target.tipo!= 10 && evt.target.tipo!=100){
								if(evt.target.tipo!= 1 && evt.target.tipo!=4 && evt.target.tipo!= 6 && evt.target.tipo!=7 && evt.target.tipo!=8 && evt.target.tipo!= 10){
									if(evt.target.isFix == false){
										evt.target.addEventListener(MouseEvent.MOUSE_MOVE,onMoveUI);
										if(returnToOriginal[evt.target.name].i){
											setChildIndex(evt.target as DisplayObject,this.numChildren-1);
										}
										
										(evt.target as Sprite).startDrag();
										this.dispatchEvent(new juegoUtilEvent(juegoUtilEvent.STARTDRAG,evt.target)); ////OSCAR:por que lo dispara cada que lo mueve???
										returnToOriginal[evt.target.name].x=evt.target.x;
										returnToOriginal[evt.target.name].y=evt.target.y;
									}
								}
							}
						} catch(e){
							
						}
					} else if(evt.type=="mouseMove" && dragger[evt.target.name]){
						//trace("mouseMove")
						lastCarrouselPosition.x=evt.target.x;
						lastCarrouselPosition.y=evt.target.y;
						//ver si esta registrado en carruseles
						if(!pauseCarrousel){
							for(var qq in carrousels){
								var els:Array=carrousels[qq].elementos;
								if(carrousels[qq].whenclick){
									for(var ww in els){
										if(els[ww]==evt.target){
											pauseCarrousel=true;
											lastCarrouselPosition.x=evt.target.x;
											lastCarrouselPosition.y=evt.target.y;
											//trace("clickeado");
										}
									}
								} else {
									var a:Array = TweenMax.getAllTweens();
									var isDC:Boolean; //is delayedCall 
									var i:int = a.length;
									while (--i > -1) {
										if(TweenLite(a[i]).target==evt.target){
											//TweenLite(a[i]).target;
											TweenCore(a[i]).paused=true;
										}
										/*isDC = (TweenLite(a[i]).target == TweenLite(a[i]).vars.onComplete);
										if (isDC == delayedCalls || isDC != tweens) {
											TweenCore(a[i]).paused = pause;
										}*/
									}
									//seguir animando banda, pero dejar de animar elemento
										if(TweenMax.isTweening(evt.target)){
											/*for(var jj in TweenMax.getTweensOf(evt.target)){
											}*/
											for(var dd in TweenMax.getAllTweens()){
												//TweenMax.getAllTweens()[dd].pauseAll();
												//TweenLite
											}
										}
								}
							}
						}
						if(dropAreas[evt.target.name]!=null){
							if(evt.target.hitTestObject(dropAreas[evt.target.name])){
								dispatchEvent(new juegoUtilEvent("collission",evt.target));
							}
						}
						
					//}
				//}
			}
		}
		
		private function soltarObjeto(evt:MouseEvent):void{
			evt.target.removeEventListener(MouseEvent.MOUSE_MOVE,onMoveUI);
			(evt.target as Sprite).stopDrag();
			try{
				if(dragger[evt.target.name]==undefined){
					dragger[evt.target.name]=false;
				}
			
			if(returnToOriginal[evt.target.name].r){
				evt.target.x=returnToOriginal[evt.target.name].x;
				evt.target.y=returnToOriginal[evt.target.name].y;
			}
			} catch(e){
				
			}
			//reactivar animacion si esq existia
			var a:Array = TweenMax.getAllTweens();
			var isDC:Boolean; //is delayedCall 
			var i:int = a.length;
			while (--i > -1) {
				if(TweenLite(a[i]).target==evt.target){
					//TweenLite(a[i]).target;
					TweenCore(a[i]).paused=false;
				}
			}
			//carrousel
			if(pauseCarrousel){
				pauseCarrousel=false;
				TweenMax.resumeAll();
				evt.target.x=lastCarrouselPosition.x;
				evt.target.y=lastCarrouselPosition.y;				
			}
			dragger[evt.target.name]=false;
			this.dispatchEvent(new juegoUtilEvent(juegoUtilEvent.STOPDRAG,evt.target));
		}
		private function identificarObjeto(evt:MouseEvent):void{
			lastClickedObject=evt.target as DisplayObject;
		}
		private function teclaAbajo(evt:KeyboardEvent):void{
			//trace(evt.keyCode);
			switch(evt.keyCode){
				case 37:
				//izquierda
				semueve(-5,0,37);
				break;
				case 39:
				//derecha
				semueve(5,0,39);
				break;
				case 38:
				//arriba
				semueve(0,-5,38);
				break;
				case 40:
				//abajo
				semueve(0,5,40);
				break;
			}
			if(actionKeys[evt.keyCode]!=undefined){
				dispatchEvent(new juegoUtilEvent("actionkey",actionKeys[evt.keyCode]));
			}
		}
		private function semueve(X:Number,Y:Number,key:int=0):void{
			var q:int=0;
			while(q<cursores.length){
				if(!cursoresPermisos[q].usemouse){
					//moverlo
					if(cursoresPermisos[q].degrees){
					if(cursoresPermisos[q].degrees  && (key==37 || key==39)){
						(cursores[q] as DisplayObject).rotation+=X;
						//trace((cursores[q] as DisplayObject).rotation)
					} else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation<0 &&(cursores[q] as DisplayObject).rotation>-90){
						//vuelta izquierda
						(cursores[q] as DisplayObject).x-=2.5;
						(cursores[q] as DisplayObject).y-=2.5;
					} else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation>0 && (cursores[q] as DisplayObject).rotation<85){
						//vuelta derecha
						(cursores[q] as DisplayObject).x+=2.5;
						(cursores[q] as DisplayObject).y-=2.5;
					} else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation>=85 && (cursores[q] as DisplayObject).rotation<95){
						//derecha
						(cursores[q] as DisplayObject).x+=2.5;
						//(cursores[q] as DisplayObject).x+=2.5;
					} else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation>=95 && (cursores[q] as DisplayObject).rotation<175){
						//derecha
						(cursores[q] as DisplayObject).y+=2.5;
						(cursores[q] as DisplayObject).x+=2.5;
					}  else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation>=175 && (cursores[q] as DisplayObject).rotation<185){
						//derecha
						(cursores[q] as DisplayObject).y+=2.5;
						//(cursores[q] as DisplayObject).x+=2.5;
					}  else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation>=185 && (cursores[q] as DisplayObject).rotation<265){
						//derecha
						(cursores[q] as DisplayObject).y+=2.5;
						(cursores[q] as DisplayObject).x-=2.5;
					}  else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation==0){
						//derecha
						(cursores[q] as DisplayObject).y-=2.5;
						//(cursores[q] as DisplayObject).x+=2.5;
					} else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation>=265 && (cursores[q] as DisplayObject).rotation<275){
						//derecha
						//(cursores[q] as DisplayObject).y-=2.5;
						(cursores[q] as DisplayObject).x-=2.5;
					} else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation<=-95 && (cursores[q] as DisplayObject).rotation>-175){
						//derecha
						(cursores[q] as DisplayObject).y+=2.5;
						(cursores[q] as DisplayObject).x-=2.5;
					}  else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation<=-85 && (cursores[q] as DisplayObject).rotation>-95){
						//derecha
						//(cursores[q] as DisplayObject).y+=2.5;
						(cursores[q] as DisplayObject).x-=2.5;
					} else if(cursoresPermisos[q].degrees  && key==38 && (cursores[q] as DisplayObject).rotation<=-175 && (cursores[q] as DisplayObject).rotation>-185){
						//derecha
						(cursores[q] as DisplayObject).y+=2.5;
						//(cursores[q] as DisplayObject).x-=2.5;
					}
					} else {
					if((cursores[q] as DisplayObject).x>cursoresPermisos[q].xmin && (cursores[q] as DisplayObject).x<cursoresPermisos[q].xmax){
						(cursores[q] as DisplayObject).x+=X;
					} else if((cursores[q] as DisplayObject).x>cursoresPermisos[q].xmin && (cursores[q] as DisplayObject).x>cursoresPermisos[q].xmax){
						(cursores[q] as DisplayObject).x=cursoresPermisos[q].xmax-1;
					} else if((cursores[q] as DisplayObject).x<cursoresPermisos[q].xmin && (cursores[q] as DisplayObject).x<cursoresPermisos[q].xmax){
						(cursores[q] as DisplayObject).x=cursoresPermisos[q].xmin+1;
					}
					if((cursores[q] as DisplayObject).y>cursoresPermisos[q].ymin && (cursores[q] as DisplayObject).y<cursoresPermisos[q].ymax){
						(cursores[q] as DisplayObject).y+=Y;
					} else if((cursores[q] as DisplayObject).y>cursoresPermisos[q].ymin && (cursores[q] as DisplayObject).y>cursoresPermisos[q].ymax){
						(cursores[q] as DisplayObject).y=cursoresPermisos[q].ymax-1;
					} else if((cursores[q] as DisplayObject).y<cursoresPermisos[q].ymin && (cursores[q] as DisplayObject).y<cursoresPermisos[q].ymax){
						(cursores[q] as DisplayObject).y=cursoresPermisos[q].ymin+1;
					}
					}
				} else {
					//checar q sea el ultimo seleccionado
					if(lastClickedObject==cursores[q]){
						(cursores[q] as DisplayObject).x+=X;
						(cursores[q] as DisplayObject).y+=Y;
					}
				}
				q++;
			}
			//this.removeEventListener(Event.ENTER_FRAME,seeHit);
			//Alert.show("end")
		}
		private function comprobarClase(elemento:*):Boolean{
			var comprobada:Boolean=true;
			for(var q:int=0;q<this._ignoredClass.length;q++){
				if(elemento is (getDefinitionByName(this._ignoredClass[q]) as Class)){
					comprobada=false;
				}
			}
			
			return comprobada;
		}
		private function comprobarNombre(elemento:DisplayObject):Boolean{
			var comprobada:Boolean=true;
			if(this._ignoredNames.indexOf(elemento.name)){
				comprobada=false;
			}
			return comprobada;
		}
		private var fakeRectangle:Rectangle;
		
		private function movimientoCascada():void{
			
		}
		protected function seeHit(evt:Event):void{
			//poner el motor de fisica
			//refreshFisix();
			caidaFisix()
			var q:int=0;
			this.dispatchEvent(new juegoUtilEvent("flashSingleCycleOperation"));
		}
		private function objetoCreado(evt:Event):void{			
			if(evt.target is FisixObject && FisixComputations[evt.target.name]==null){
				//trace("registrando fisix:"+this.getBounds(evt.target as FisixObject)+" name:"+evt.target.name);
				//trace(evt.target.name)
				FisixComputations[evt.target.name]=this.getBounds(evt.target as FisixObject);
			}			
		}
		private function objetoDestruido(evt:Event):void{
			if(evt.target is FisixObject){
			}
		}
		
		private function caidaFisix():void{	
			//trace("entra enterframe")
			for each(var obj:Object in this.listCuadritos){
				_dummyCounter=0;
				while(this._dummyCounter<obj.list.length){
					if(obj.list[_dummyCounter][1] is FisixObject){
						   var elemento:FisixObject = obj.list[_dummyCounter][1] as FisixObject;
						   if(!elemento.estatico){
							  _dummyCounter2=0;
							//  trace('ENTRA ENTER FRAME')
								if(!elemento.pasivo){
									elemento.y+=(this.GRAVITY);
									//trace('ENTRA ENTER FRAME '+   CuadroGrafico(obj.list[_dummyCounter2][1]))
									if(elemento.name == 'piso'){
										elemento.y = 325;
									}
								}
								while(this._dummyCounter2<obj.list.length){							
									if(obj.list[_dummyCounter2][1] != elemento){
										if(elemento.hitTestObject(obj.list[_dummyCounter2][1]) && (obj.list[_dummyCounter2][1] is FisixObject) && !elemento.pasivo && !(obj.list[_dummyCounter2][1] as FisixObject).pasivo && !elemento.estatico){
											elemento.y= obj.list[_dummyCounter2][1].y-(elemento.height+1);
										}
									}
									_dummyCounter2++;
								}							
								for each(var layout:Object in this.listLayout){	
									if((layout[1] is BaseLayout) && elemento.hitTestObject(layout[1])){
										(layout[1] as BaseLayout).centrar(elemento);
									}
								}							
						}
					}
					_dummyCounter++;
				}	
			}			
		}
		
		
		/////////////////////////////////////////////////////////////////////////////////
		private function refreshFisix():void{
			//logica de motor de fisica
			this._displaysTotales=this.numChildren;
			var div:int = _displaysTotales/20
			_dummyCounter=0;			
			while(this._dummyCounter<this._displaysTotales){
				if((this.getChildAt(_dummyCounter) is FisixObject)){
					var elemento:FisixObject=this.getChildAt(_dummyCounter) as FisixObject;
					//trace(elemento.name)
					if(!elemento.estatico){
						//caida libre
						_dummyCounter2=0;
						if(!elemento.pasivo){
							elemento.y+=(this.GRAVITY);
						}
						while(this._dummyCounter2<this._displaysTotales){							
							if(this.getChildAt(_dummyCounter2) != elemento){
								if((elemento.hitTestObject(this.getChildAt(_dummyCounter2)) && (this.getChildAt(_dummyCounter2) is FisixObject) && !elemento.pasivo && !(this.getChildAt(_dummyCounter2) as FisixObject).pasivo && !elemento.estatico) || (this.getChildAt(_dummyCounter2).hitTestObject(elemento))){
									elemento.y=this.getChildAt(_dummyCounter2).y-(elemento.height+1);
								} else if((this.getChildAt(_dummyCounter2) is BaseLayout) && elemento.hitTestObject(this.getChildAt(_dummyCounter2))){
									(this.getChildAt(_dummyCounter2) as BaseLayout).centrar(elemento);
								}
							}
							_dummyCounter2++;
						}
					}
				}
				_dummyCounter++;
			}	
		}
	}
	
		
	
	
}