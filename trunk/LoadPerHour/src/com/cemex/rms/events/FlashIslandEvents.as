package com.cemex.rms.events
{
	import flash.events.Event;
	
	public class FlashIslandEvents extends Event
	{
		public static const SHOW_GRAPH:String = "SHOW_GRAPH";
		public static const SHOW_GRAPH_REFRESH:String = "SHOW_GRAPH_REFRESH";
		public static const SHOW_MEP:String = "SHOW_MEP";
		public static const SHOW_DETAIL:String = "SHOW_DETAIL";
		public static const PLANT_SIM_PARAM:String = "PLANT_SIM_PARAM";
		public static const CONFIRM_CHANGES:String = "CONFIRM_CHANGES";
		public static const SAVE_DATA:String = "SAVE_DATA";
		public static const PLANT_SELECTED:String = "PLANT_SELECTED";
		//public static const MOVE_FULL_ORDER_ERR:String = "MOVE_FULL_ORDER_ERR";//movimiento completo con pattern
		//public static const MOVE_FULL_ORDER_OK:String = "MOVE_FULL_ORDER_OK";//movimiento completo, requestMovOrd
		public static const MEP_SELECTED:String = "MEP_SELECTED";
		public static const SHOW_SUCCESS_MSG_CHG_PLT:String = "SHOW_SUCCESS_MSG_CHG_PLT";
		public static const SHOW_ERR_MSG_CHG_PLT:String = "SHOW_ERR_MSG_CHG_PLT";
		public static const TRAE:String = "TRAE";
		public static const MEP_SELECT_PLANT:String="MEP_SELECT_PLANT";
		public static const SET_VEHICLES:String="SET_VEHICLES";
		public static const MOVE_ONE_ORDER_OK:String="MOVE_ONE_ORDER_OK";
		public static const MOVE_ONE_ORDER_ERR:String="MOVE_ONE_ORDER_ERR";
		public static const MOVE_FULL_ORDER_OK:String="MOVE_FULL_ORDER_OK";
		public static const MOVE_FULL_ORDER_ERR:String="MOVE_FULL_ORDER_ERR";
		public static const CANCEL_MOVE:String="CANCEL_MOVE";
		public static const SHOW_SIMULATION_POPUP:String="SHOW_SIMULATION_POPUP";
		
		public var data:*;
		public function FlashIslandEvents(type:String, datos:*, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			data=datos;
		}
	}
}
/*
changeSAKES(p_is_ok)
get color 
s¡ es ok
crear olumna como esta ( color)

si no 
crear columna( color x -1) */