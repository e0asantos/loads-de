package com.cemex.rms
{
	public class Constants
	{
		public static const PLANT:String="PLANT";
		public static const FRECUENCY:String="FRECUENCY";
		public static const LOADING_TIME:String="LOADING_TIME";
		public static const VOLUME:String="VOLUME";
		public static const CHANGE_TYPE:String="CHANGE_TYPE";
		public static const ITEM_NUMBER:String="ITEM_NUMBER";
		public static const NEW_VALUE:String="NEW_VALUE";
		public static const ORDER_NUMBER:String="ORDER_NUMBER";
		public static const PRE_VALUE:String="PRE_VALUE";
		public static const CAPACITY_VEHICLE:String="CAPACITY_VEHICLE";
		public static const PARAM:String="PARAM";
		public static const CHANGE_RECORD:String="CHANGE_RECORD";
		public static const VALUE:String="VALUE";
		public static const CHG_PLT:String="CHG_PLT";
		public static const WERKS:String="WERKS";
		public static const IS_FULL:String="IS_FULL";
		
		public static const NUM_ITEMS_CHANGED:String="NUM_ITEMS_CHANGED";
		public static const MAX_LOAD_M3:String="MAX_LOAD_M3";
		public static const PLT_HIGH_LIMIT_P:String="PLT_HIGH_LIMIT_P";
		public static const NAME1:String="NAME1";
		
		public static const SEL_LTRAV:String="SEL_LTRAV";
		public static const SEL_WERKS:String="SEL_WERKS";
		public static const PLANT_LIST:String="Plant list";
		
		
		/***************MENSAJES************************/
		public static const SELECTED_PLANT:String="Selected plant:";
		public static const SELECTED_REQUISITION:String="Selected requisition:";
		public static const SELECTED_POSITION:String="Selected position:";
		public static const SELECTED_LOAD:String="Selected load:";
		public static const SELECTED_ORDER:String="Selected order:";
		public static const PENDING:String="Pending";
		public static const IN_PROGRESS:String="In progress:";
		public static const COMPLETED:String="Completed";
		public static const CHANGE_HOUR:String="Cambio hora";
		public static const CHANGE_PLANT:String="Cambio planta";
		
		
		
		
		
		public static const CHG_HR:String="CHG_HR";
		public static const VBELN_SELECTED:String="VBELN_SELECTED";
		public static const POSNR_SELECTED:String="POSNR_SELECTED";
		public static const WERKS_SELECTED:String="WERKS_SELECTED";
		public static const PLANT_SELECTED:String="PLANT_SELECTED";
		public static const PI_VBELN:String="PI_VBELN";
		public static const PI_POSNR:String="PI_POSNR";
		public static const PI_WERKS:String="PI_WERKS";
		public static const PI_PLANT:String="PI_PLANT";
		
		public static const VBELN:String="VBELN";
		public static const POSNR:String="POSNR";
		public static const UATEN:String="UATEN";
		public static const CHANGE_TYPE2:String="CHANGE_TYPE2";
		
		
		public function Constants()
		{
			
		}
	}
}