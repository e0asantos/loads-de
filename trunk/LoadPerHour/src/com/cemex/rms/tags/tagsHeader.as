package com.cemex.rms.tags
{
	[Bindable]
	public class tagsHeader
	{
		public var plant:String = '';
		public var pending:String = '';
		public var progress:String = '';
		public var completed:String = '';
		public var blocked:String = '';
		
		public var plantList:String = '';
		public var showLog:String = '';
		public var log:String	= '';
		public var changeType:String = '';
		public var newValue:String = '';
		public var previousValue:String = '';
		public var totals:String="";
		public var unloadingTime:String="";
		public var capacity:String="";
		public var unitOfMeasure:String="";
		public var journeyTime:String="";
		
		
		public function tagsHeader()
		{
		}
	}
}