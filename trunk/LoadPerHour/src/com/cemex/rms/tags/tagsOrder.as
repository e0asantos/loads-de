package com.cemex.rms.tags
{

	[Bindable]
	public class tagsOrder
	{
		public var number:String = '';
		public var numberLoad:String = '';
		public var status:String = '';
		public var product:String = '';
		public var deliveryTime:String = '';
		public var loadingTime:String = '';
		public var fix:String = '';
		public var optPlant:String = '';
		public var message:String = '';
		public var loadh_invalid_delivery_time_msg:String='';
		public var loadh_invalid_unloading_time_msg:String='';
		public var loadh_invalid_travel_time_msg:String='';
		public function tagsOrder()
		{
		}
	}
}