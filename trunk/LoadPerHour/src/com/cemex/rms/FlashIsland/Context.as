package com.cemex.rms.FlashIsland
{
	import com.cemex.rms.events.FlashIslandEvents;
	import com.cemex.rms.tags.tagsSingleton;
	import com.cemex.rms.utils.ChartUtil;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	[Event(name="SHOW_GRAPH",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="REFRESH_FILTERS",type="com.cemex.rms.events.FlashIslandEvents")]

	[Event(name="SHOW_MEP",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="SHOW_DETAIL",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="PLANT_SIM_PARAM",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="CONFIRM_CHANGES",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="SAVE_DATA",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="PLANT_SELECTED",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="MOVE_FULL_ORDER",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="SHOW_SUCCESS_MSG_CHG_PLT",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="SHOW_ERR_MSG_CHG_PLT",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="MEP_SELECT_PLANT",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="SET_VEHICLES",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="MOVE_ONE_ORDER_OK",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="MOVE_ONE_ORDER_ERR",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="MOVE_FULL_ORDER_OK",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="MOVE_FULL_ORDER_ERR",type="com.cemex.rms.events.FlashIslandEvents")]
	
	[Event(name="SHOW_SIMULATION_POPUP",type="com.cemex.rms.events.FlashIslandEvents")]
	
	public class Context extends EventDispatcher
	{
		public function Context()
		{
			super();
		}
		/*******VARS*****/
		[Bindable]
		public  var __webdynproReady:Boolean = false;
		[Bindable]
		public var __lcds:Object;
		
		[Bindable]
		public var __dataSource:ArrayCollection;
		[Bindable]
		public var __dataSourceMaxPlant:ArrayCollection;
		[Bindable]
		public var __dataSourceStatus:ArrayCollection;
		[Bindable]
		public var __dataSourceGlobalData:Object;
		[Bindable]
		public var __dataSourceAnOrderDetail:ArrayCollection;
		[Bindable]
		public var __dataSourceLinesDetail:ArrayCollection;
		[Bindable]
		public var __dataSourceSimParams:ArrayCollection;
		[Bindable]
		public var __dataSourceItemsChanged:ArrayCollection;
		[Bindable]
		public var __dataSourceInputMEP:Object;
		[Bindable]
		public var __dataSourceOutputMEP:Object;
		[Bindable]
		public var __dataSourceVehicleAv:ArrayCollection;
		[Bindable]
		public var __dataSourceFieldDescOrders:ArrayCollection;
		[Bindable]
		public var __dataSourceFieldDescDetail:Object;
		[Bindable]
		public var __dataSourceOutputMEP2:Object;
		
		[Bindable]
		public var __itemsChangeDos:ArrayCollection;
		
		[Bindable]
		public var __prueba:Object;
		
		[Bindable]
		public var __FX_TVARV:Object;
		
		private var _show_flex_popup:String;

		
		
		
		private var _counts:int=0;
		/************************DATA SOURCES GET************************/
		public function set show_flex_popup(value:String):void{
			_show_flex_popup=value;
			if(value=="yes"){
				//var mvar1:String=value;
				this.dispatchEvent(new FlashIslandEvents(FlashIslandEvents.SHOW_SIMULATION_POPUP,null));
			}
		}
		
		public function get show_flex_popup():String{
			return _show_flex_popup;
		}
		
		public function get WEBDYNPRO_READY():Boolean{
			return __webdynproReady;
		}
		public function get LCDS():Object{
			return __lcds;
		}
		public function get dataSource():ArrayCollection{
			//trace("GET DATASOURCE");
			return __dataSource;
		}
		public function get itemsChangeDos():ArrayCollection{
			return __itemsChangeDos;
		}
		public function get dataSourceMaxPlant():ArrayCollection{
			return __dataSourceMaxPlant;
		}
		
		public function get dataSourceStatus():ArrayCollection{
			return __dataSourceStatus;
		}
		public function get dataSourceGlobalData():Object{
			return __dataSourceGlobalData;
		}
		public function get dataSourceAnOrderDetail():ArrayCollection{
			return __dataSourceAnOrderDetail;
		}
		public function get dataSourceLinesDetail():ArrayCollection{
			return __dataSourceLinesDetail;
		}
		public function get dataSourceSimParams():ArrayCollection{
			return __dataSourceSimParams;
		}
		public function get dataSourceItemsChanged():ArrayCollection{
			return __dataSourceItemsChanged;
		}
		public function get dataSourceInputMEP():Object{
			return __dataSourceInputMEP;
		}
		public function get dataSourceOutputMEP():Object{
			return __dataSourceOutputMEP;
		}
		public function set dataSourceOutputMEP2(value:Object):void{
			 __dataSourceOutputMEP2=value;
		}
		public function get dataSourceOutputMEP2():Object{
			return __dataSourceOutputMEP2;
		}
		//public function get dataSourceVehicleAv():ArrayCollection{
		public function get VEHIC_PER_HOUR():ArrayCollection{
			return __dataSourceVehicleAv;
		}
		public function get dataSourceFieldDescOrders():ArrayCollection{
			return __dataSourceFieldDescOrders;
		}
		public function get dataSourceFieldDescDetail():Object{
			return __dataSourceFieldDescDetail;
		}
		public function get FX_TVARV():Object{
			return __FX_TVARV;
		}
		public function get PRUEBA():Object{
			return __prueba;
		}
		public var __cmpNode:Object;
		public function get cmpNode():Object{
			return __cmpNode;
		}
		public function set cmpNode(value:Object):void{
			__cmpNode=value;
		}
		/**************************DATA SOURCES SET**********************/
		public function set FX_TVARV(value:Object):void{
			__FX_TVARV=value;
		}
		public var _otrlabels:Object;
		public function set dataSourceOTRLabels(value:ArrayCollection):void{
			_otrlabels=value;
			var tag:tagsSingleton = tagsSingleton.getInstance();
			
			
			for(var obj:Object in value){
				//search fot the tvarvc values
				if(value[obj].ALIAS == 'COMPLETED'){
					ChartUtil._completed.push(value[obj].VALUE);
				}
				if(value[obj].ALIAS == 'CANCELLED'){
					ChartUtil._cancelled.push(value[obj].VALUE);
				}
				if(value[obj].ALIAS == 'PENDING'){
					ChartUtil._pending.push(value[obj].VALUE);
				}
				if(value[obj].ALIAS == 'CONFIRMED'){
					ChartUtil._confirmed.push(value[obj].VALUE);
				}
				if(value[obj].ALIAS == 'PROGRESS'){
					ChartUtil._progress.push(value[obj].VALUE);
				}
				if(value[obj].ALIAS == 'BLOCKED'){ 
					ChartUtil._blocked.push(value[obj].VALUE);
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_PLANT_LIST'){
					tag.header.plantList = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_TOTAL'){
					tag.header.totals = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_SHOW_LOG'){
					tag.header.showLog = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_LOG'){
					tag.header.log = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_CHANGE_TYPE'){
					tag.header.changeType = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_NEW_VALUE'){
					tag.header.newValue = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_PREVIOUS_VALUE'){
					tag.header.previousValue = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_UNLOADING_TIME'){
					tag.header.unloadingTime = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_CAPACITY'){
					tag.header.capacity = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_JOURNEY_TIME'){
					tag.header.journeyTime = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_UNIT_OF_MEASURE'){
					tag.header.unitOfMeasure = value[obj].VALUE;
				}
				////trace(value[obj].ALIAS)
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ALERT_NOMOVEORDERSTATUS'){
					tag.alert.noMoveOrderStatus = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_MSG_CANCELLED'){
					tag.alert.msgCancelled = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_MSG_CONFIRMED'){
					tag.alert.msgConfirmed = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ALERT_NOMOVEORDERSTATUSFIX'){
					tag.alert.noMoveOrderFix = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ALERT_NOMOVEORDERDATE'){
					tag.alert.noMoveOrderDate = value[obj].VALUE;
				}				
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_CLIENT'){
					tag.internalComm.clientName = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_COMENTARY'){
					tag.internalComm.internalComentary = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_DELIVERYTIME'){
					tag.internalComm.deliveryTime = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_DISCHARGEDMETHOD'){
					tag.internalComm.dischargMethod = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_ELEMENTTOCOLAR'){
					tag.internalComm.elementColar = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_FREQUENCY'){
					tag.internalComm.frequency = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_LOADINGTIME'){
					tag.internalComm.loadingTime = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_NAMEFRONT'){
					tag.internalComm.frontName = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_NUMBERLOAD'){
					tag.internalComm.numberLoad = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_NUMBERORDER'){
					tag.internalComm.numberOrder = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_OK'){
					tag.internalComm.buttonLabel = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_PRIORITY'){
					tag.internalComm.priority = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_PRODUCT'){
					tag.internalComm.product = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_PROPERTY'){
					tag.internalComm.property = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_PROPERTYVALUE'){
					tag.internalComm.result = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_TYPE'){
					tag.internalComm.type = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_RETCODE'){
					tag.internalComm.retCode = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_PUMP'){
					tag.internalComm.pump = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_PUMPMAX'){
					tag.internalComm.pumpMax = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_DETAIL_PIPE'){
					tag.internalComm.pipe = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_HEADER_COMPLETED'){
					tag.header.completed = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_HEADER_PENDING'){
					tag.header.pending = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_HEADER_PROGRESS'){
					tag.header.progress = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_HEADER_PLANT'){
					tag.header.plant = value[obj].VALUE;	
				}
				
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_DELIVERYTIME'){
					tag.order.deliveryTime = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_LOADINGTIME'){
					tag.order.loadingTime = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_NUMBER'){
					tag.order.number = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_NUMBER_LOAD'){
					tag.order.numberLoad = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_PRODUCT'){
					tag.order.product = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_OPTIMAPLANT'){
					tag.order.optPlant = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_MESSAGE'){
					tag.order.message = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_STATUS'){
					tag.order.status = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_ORDER_FIX'){
					tag.order.fix = value[obj].VALUE;	
				}				
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_PUSH_LOADMOVE'){
					tag.push.msgLoadMove = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_PUSH_ORDERMOVE'){
					tag.push.msgOrderMove = value[obj].VALUE;	
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_SELECTED_LOAD'){
					tag.itemSel.load = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_SELECTED_ORDER'){
					tag.itemSel.order = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_SELECTED_PLANT'){
					tag.itemSel.plant = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_INVALID_TRAVEL_TIME_MSG'){
					tag.order.loadh_invalid_travel_time_msg = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_INVALID_UNLOADING_TIME_MSG'){
					tag.order.loadh_invalid_unloading_time_msg = value[obj].VALUE;
				}
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_INVALID_DELIVERY_TIME_MSG'){
					tag.order.loadh_invalid_delivery_time_msg= value[obj].VALUE;
				}
				
				if(value[obj].ALIAS == 'ZMXCSDSLS/LPH_MSG_BLOCKED'){
					tag.header.blocked = value[obj].VALUE;
				}
			}
			
		}
		public function set WEBDYNPRO_READY(data:Boolean):void{
			this.__webdynproReady = data;
		}
		public function set LCDS(value:Object):void{
			__lcds = value;
		}
		public function set dataSource(value:ArrayCollection):void{
			loadCount();
			__dataSource=value;
		}
		public function set dataSourceMaxPlant(value:ArrayCollection):void{
			loadCount();
			__dataSourceMaxPlant=value;
		}
		
		public function set dataSourceStatus(value:ArrayCollection):void{
			loadCount();
			 __dataSourceStatus=value;
		}
		public function set dataSourceGlobalData(value:Object):void{
			loadCount();
			__dataSourceGlobalData=value;
			if(value.hasOwnProperty("ACTION_EXECUTED")){
				if(value["ACTION_EXECUTED"]!="" && value["ACTION_EXECUTED"]!=null){
					//trace("EVENTO:"+value["ACTION_EXECUTED"]);
					this.dispatchEvent(new FlashIslandEvents(value["ACTION_EXECUTED"],__dataSource));
				}
			}
			if(value.hasOwnProperty("MOVE_FULL_ORDER")){
				if(value["MOVE_FULL_ORDER"]!="" && value["MOVE_FULL_ORDER"]!=null){
					//trace("EVENTO MOVE FULL ORDER:"+value["MOVE_FULL_ORDER"]);
					//this.dispatchEvent(new FlashIslandEvents(value["ACTION_EXECUTED"],__dataSource));
				}
			}
			 
		}
		public function set itemsChangeDos(valor:ArrayCollection):void{
			__itemsChangeDos=valor;
		}
		public function set dataSourceAnOrderDetail(value:ArrayCollection):void{
			loadCount();
			 __dataSourceAnOrderDetail=value;
		}
		public function set dataSourceLinesDetail(value:ArrayCollection):void{
			loadCount();
			 __dataSourceLinesDetail=value;
		}
		public function set dataSourceSimParams(value:ArrayCollection):void{
			
			loadCount();
			 __dataSourceSimParams=value;
		}
		public function set dataSourceItemsChanged(value:ArrayCollection):void{
			loadCount();
			 __dataSourceItemsChanged=value;
		}
		public function set dataSourceInputMEP(value:Object):void{
			loadCount();
			 __dataSourceInputMEP=value;
		}
		public function set dataSourceOutputMEP(value:Object):void{
			loadCount();
			 __dataSourceOutputMEP=value;
		}
		//public function set dataSourceVehicleAv(value:ArrayCollection):void{
		//public function set VEHIC_PER_HOUR(value:ArrayCollection):void{
		public function set VEHIC_PER_HOUR(value:ArrayCollection):void{
			__dataSourceVehicleAv=value;			
			if(value.length>0){
				loadCount("ve");
			}
		}
		public function set dataSourceFieldDescOrders(value:ArrayCollection):void{
			loadCount();
			 __dataSourceFieldDescOrders=value;
		}
		public function set dataSourceFieldDescDetail(value:Object):void{
			loadCount();
			 __dataSourceFieldDescDetail=value;
		}
		
		public function set PRUEBA(evt:Object):void{
			for (var q:Object in evt){
				//trace("======");
				//trace(q+":"+evt[q]);
				//trace("======");
			}
			__prueba=evt;
		}
		
		[Bindable]
		public var __solo:Object;
		
		public function set SOLO(value:Object):void{
			__solo=value;
		}
		public function get SOLO():Object{
			return __solo;
		}
		public function loadCount(force:String=""):void{
			_counts++;
			if(force=="ve"){
				this.dispatchEvent(new FlashIslandEvents(FlashIslandEvents.SET_VEHICLES,1));
			}
			////trace(_counts);
		}
		public function executeSimulationParams():void{
			this.dispatchEvent(new FlashIslandEvents(FlashIslandEvents.PLANT_SIM_PARAM,null));
		}
		
		
		
	}
}