package com.cemex.util
{
	import mx.collections.ArrayCollection;

	public interface IObserver
	{
		function receiveSubjectCommand(command:String, params:*=null):void;
	}
}