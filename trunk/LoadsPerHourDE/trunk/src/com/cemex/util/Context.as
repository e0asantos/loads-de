package com.cemex.util
{
	

	public class Context
	{
		private static var _instance:Context;
		public var _maxplantload:Number;
		private var listeners:Array;
		
		public const SHOW_TOOLTIP:String = "show_tooltip";
		public const HIDE_TOOLTIP:String = "hide_tooltip";
		public const ALERT_SERIES:String = "alert_series";
		public const RESET_SERIES:String = "reset_series";
		public const HIGHLIGHT_ORDER:String = "highlight_order";
		public const HIGHLIGHT_ALL:String = "highlight_all";
		public const ALPHA_LEVEL_ALERT:String = "0.4"
		public const ALPHA_LEVEL_OFF:String = "0"		
		
		public const HIGHLIGHT_GRID_ORDER:String = "highlight_grid_order";
		
		public function Context(e:Enforcer)  {
			if(e == null)
				throw new Error("Clase pseudo-Singleton, usar getInstance()");
		}
		
		public function getmaxplantload():Number{
			return _maxplantload;
		}
		
		public function setmaxplantload(value:Number):void{
			_maxplantload = value;
		}
		
		public static function getInstance():Context {
			if(_instance == null)
				_instance = new Context (new Enforcer);
			return _instance;
		}	
		
		public function suscribListener(suscriber:IObserver):void{
			if (listeners==null)
				listeners = new Array();
			listeners.push(suscriber);
		}
		
		public function unsuscribe(suscriber:IObserver):void{
			if (listeners==null)
				return;
			for(var i:int = 0; i < listeners.length; i++){
				if(suscriber === listeners[i]){
					listeners.splice(i, 1);
					break;
				}
			}
		}
		
		public function command2Listeners(eventType:String, params:*=null):void{
			if (listeners==null)
				return;
			for (var x:int = 0; x< listeners.length;x++){
				(listeners[x] as IObserver).receiveSubjectCommand(eventType, params);
			}
		}
		
	}
}
class Enforcer{}