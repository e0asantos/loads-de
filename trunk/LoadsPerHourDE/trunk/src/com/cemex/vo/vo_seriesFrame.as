package com.cemex.vo
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class vo_seriesFrame
	{
		public var id:String;
		public var width:Number;
		public var height:Number;
		public var backGroundColor:String;
		public var subItemsCount:Number;
		public var subItemsColl:ArrayCollection;
		
		public function vo_seriesFrame()
		{
			 id = "";
			 width = 0;
			 height = 0;
			 backGroundColor = "";
			 subItemsCount = 0;
			 subItemsColl = new ArrayCollection();			
		}
	}
}