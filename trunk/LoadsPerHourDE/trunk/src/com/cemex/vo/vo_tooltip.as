package com.cemex.vo
{
	[Bindable]
	public class vo_tooltip
	{
		public var orderNumber:String;
		public var item:String;
		public var deliveryGroup:String;
		public var status:String;
		public var product:String;
		public var comments:String;
		public var deliveryTime:String;
		public var loadingTime:String;
		public var fixOption:String;
		public var optimalPlant:String;
		public var client:String;
		public var pointOfDelivery:String;
		public var volume:String;
		public var load:String;
		public var etenr:String;

		public function vo_tooltip()
		{
			 orderNumber = "";
			 item = "";
			 etenr="";
			 load="";
			 deliveryGroup = "";
			 status = "";
			 product = "";
			 comments = "";
			 deliveryTime = "";
			 loadingTime = "";
			 fixOption = "";
			 optimalPlant = "";
			 client = "";
			 pointOfDelivery = "";
			 volume = "";
		}
	}
}