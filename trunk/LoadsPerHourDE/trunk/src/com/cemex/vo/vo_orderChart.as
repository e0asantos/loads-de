package com.cemex.vo
{
	[Bindable]
	public class vo_orderChart
	{
		public var order_number:String;
		public var item:String;
		public var status:String;
		public var loading_time:String;
		public var loading_date:String;
		public var load_volume:String;
		public var border_color:String;
		public var fill_color:String;
		public var etenr:String;
		
		public function vo_orderChart()
		{
			order_number = "";
			item = "";
			status = "";
			loading_time = "";
			loading_date = "";
			load_volume = "";
			border_color = "";
			fill_color = "";
		}
	}
}