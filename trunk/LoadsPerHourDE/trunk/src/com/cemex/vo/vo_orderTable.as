package com.cemex.vo
{
	[Bindable]
	public class vo_orderTable
	{
		public var order_number:String;
		public var load_time:String;
		public var deliv_time:String;
		public var volume:String;
		public var truck_vol:String;
		public var frequency:String;
		public var job_site:String;
		public var comments:String;
		public var order_color:String;
		public var instructions:String;
		public var cust_name:String;
		public var etenr:String;
		
		public function vo_orderTable()
		{
			order_number = "";
			load_time = "";
			deliv_time = "";
			volume = "";
			truck_vol = "";
			frequency = "";
			job_site = "";
			comments = "";
			order_color = "";
			instructions="";
			cust_name="";
		}
	}
}