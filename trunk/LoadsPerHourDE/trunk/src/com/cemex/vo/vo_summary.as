package com.cemex.vo
{
	[Bindable]
	public class vo_summary
	{
		public var total_delivered:Number;
		public var total_pending:Number;
		public var total_orders:Number;
		public var total_volume:Number;
		
		public function vo_summary()
		{
			total_delivered = 0;
			total_pending = 0;
			total_orders = 0;
			total_volume = 0;
		}
	}
}