package customChartAnnotations {
	
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	
	import mx.charts.chartClasses.*;
	import mx.controls.*;
	
	public class Line extends ChartElement {
		
		/* Parámetros */
		public var horizontal:Boolean = true;
		public var X:Number;
		public var Y:Number;
		public var largoLinea:Number = 100;
		public var lineColor:uint;
		public var anchoLinea:Number = 3;

		public function Line():void{
			
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth, unscaledHeight);

			var g:Graphics = graphics;
			g.clear();
			
			var c:Array;
			
			if (horizontal){
				c = [{dx: -1, dy: Y}, {dx:largoLinea, dy: Y}];
			}else{
				c = [{dx: X, dy: largoLinea}, {dx:X, dy: 0}];
			}
			
			dataTransform.transformCache(c,"dx","x","dy","y");

			g.moveTo(c[0].x,c[0].y);				
			g.beginFill(0xFFFFFF,.2);
			g.lineStyle(anchoLinea,lineColor);
			g.drawRect(c[0].x,c[0].y,c[1].x - c[0].x, c[1].y - c[0].y);
			g.endFill();
	
		}

		override public function describeData(dimension:String,requiredFields:uint):Array{
			return new Array();
		}

		override protected function commitProperties():void{	
			super.commitProperties();
			
//			/* when our data changes, we need to update the text displayed in our labels */
//			_labelLeft.text = Math.round(dLeft).toString();
//			_labelRight.text = Math.round(dRight).toString();
//			_labelTop.text = Math.round(dTop).toString();
//			_labelBottom.text = Math.round(dBottom).toString();
			
		}

		public function updateLocation():void{
			updateDisplayList(0,0);
		}

		override public function mappingChanged():void{
			invalidateDisplayList();
		}
	}
}
