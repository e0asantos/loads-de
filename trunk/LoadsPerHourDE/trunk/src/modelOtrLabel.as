package
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class modelOtrLabel
	{
		public var lblPlant:String = '';
		public var lblOrderNumber:String = '';
		public var lblLoadTime:String = '';
		public var lblDeliveryTime:String = '';
		public var lblVolume:String = '';
		public var lblTrukCapacity:String = '';
		public var lblFrecuency:String = '';
		public var lblJobSite:String = '';
		public var lblComments:String = '';
		
		static private var _instance:modelOtrLabel = null;	
		static public function getInstance():modelOtrLabel{
			if(_instance == null){
				_instance = new modelOtrLabel();
			}
			return _instance;
		}
		
		
		public function dataSourceOTRLabels(value:ArrayCollection):void{
			for each(var val:Object in value){
				switch(val.ALIAS){
					case 'ZDECSDSLS/CHARTLPH_PLANT':
						lblPlant = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_ORDERNUMBER':
						lblOrderNumber = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_LOADTIME':
						lblLoadTime = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_DELIVERYTIME':
						lblDeliveryTime = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_VOLUME':
						lblVolume = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_TRUKCAPACITY':
						lblTrukCapacity = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_FRECUENCY':
						lblFrecuency = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_JOBSITE':
						lblJobSite = val.VALUE;
						break;
					case 'ZDECSDSLS/CHARTLPH_COMMENTS':
						lblComments = val.VALUE;
						break;
						
				}
			}
		}
	}
}