package
{
	import flash.events.Event;
	
	public class eventEndEffect extends Event
	{
		public static const END_EFFECT_HIDE_ITEM:String = 'endEffect'
		public static const INI_EFFECT_HIDE_ITEM:String = 'iniEffect'
			
		public var chartType:String;
		public function eventEndEffect(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(END_EFFECT_HIDE_ITEM, bubbles, cancelable);
		}
	}
}