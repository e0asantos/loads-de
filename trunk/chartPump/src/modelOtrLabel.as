package
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class modelOtrLabel
	{
		public var lblPumpSchedule:String = ''
		public var lblWhitOutput:String = ''
		public var lblPumpTruck:String = ''
		public var lblOrderNumber:String = ''
		public var lblPumpOrder:String = ''
		public var lblPumpVehicle:String = ''
		public var lblPumpType:String = ''
		public var lblPumpReach:String = ''
		public var lblPumpOrderStatus:String = ''
		public var lblPumpVolume:String = ''
		public var lblStartTime:String = ''
		public var lblEndTime:String = ''
		public var lblJobSite:String = ''
		public var lblJobAddress:String = ''
		public var lblOrderBy:String = ''
		public var lblOrderAscending:String = ''
		public var lblOrderDescending:String = ''
		
		static private var _instance:modelOtrLabel = null;	
		static public function getInstance():modelOtrLabel{
			if(_instance == null){
				_instance = new modelOtrLabel();
			}
			return _instance;
		}
		
		
		public function dataSourceOTRLabels(value:ArrayCollection):void{
			for each(var val:Object in value){
				switch(val.ALIAS){
					case 'ZMXCSDSLS/CHARTPUMP_PUMPSCHEDULE':
						lblPumpSchedule = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_WITHOUTPUT':
						lblWhitOutput = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_PUMPTRUCK':
						lblPumpTruck = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_ORDERNUMBER':
						lblOrderNumber = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_PUMPORDER':
						lblPumpOrder = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_PUMPVEHICLE':
						lblPumpVehicle = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_PUMPTYPE':
						lblPumpType = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_PUMPREACH':
						lblPumpReach = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_PUMPORDERSTATUS':
						lblPumpOrderStatus = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_PUMPVOLUME':
						lblPumpVolume = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_STARTTIME':
						lblStartTime = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_ENDTIME':
						lblEndTime = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_JOBSITE':
						lblJobSite = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_JOBADDRESS':
						lblJobAddress = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_ORDERBY':
						lblOrderBy = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_ORDERASCENDING':
						lblOrderAscending = val.VALUE;
						break;
					case 'ZMXCSDSLS/CHARTPUMP_ORDERDESCENDING':
						lblOrderDescending = val.VALUE;
						break;
				}
			}
		}
	}
}