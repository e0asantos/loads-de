package
{
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	
	/**
	 * Order List Class. order any list in ascending or descending order according to property that is defined on the type of object that is contained in that list.
	 *//* @author <ul>
	 *         <li><a href="mailto:odcp@hotmail.com">Oscar Camargo</a></li>
	 *         <li><a href="mailto:omar.palomino@neoris.com">Omar Palomino</a></li>
	 *         </ul>
	 */
	public class setOrderList
	{
		/**
		 * Constructor setOrderList.
		 * @param list List to Order.
		 * @param orderBy Specifies the object property list that is ordered.
		 * @param descending Define the order will be ascending or descending.
		 */
		public function setOrderList(list:ArrayCollection, orderBy:String, descending:Boolean = false)
		{
			var orderList:Sort = new Sort();
			orderList.fields = [new SortField(orderBy, true, descending)]
			list.sort = orderList;
			list.refresh();
		}
	}
	
}