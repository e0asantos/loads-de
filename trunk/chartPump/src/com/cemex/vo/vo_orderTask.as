package com.cemex.vo
{
	[Bindable]
	public class vo_orderTask
	{
		public var keyName:String = '';
		
		public var resourceId:String = '';
		public var orderNumber:String = '';
		
		public var plant:String = '';
		
		public var orderStatus:String = '';
		public var orderColor:Number = 0x44ff00;
		public var pumpOrder:String = '';
		public var pumpVehicle:String = '';
		public var pumpVolumen:String = '';
		public var pumpType:String = '';
		public var pumpReach:String = '';
		public var pumpOrderStatus:String = '';
		public var pumpRenegotiation:String = '';
		public var timeDiff:Number = 0;
		public var startTime:Date = new Date();
		public var endTime:Date = new Date();
		public var startTimeLabel:Date = new Date();
		public var endTimeLabel:Date = new Date();
		public var subtractTime:Date = new Date();
		public var jobSite:String = '';
		public var jobAddress:String = '';
		
		public var pumpTimeTotal:Date = new Date();		
		public var pumpTotalTime:String = '';
		public var pumpInstallingTime:String = '';
		public var pumpingTime:String = '';
		
		public var showing:String = 'showIn';
		
		public var percentWidth:Number  = 100;
		public var percentHeight:Number = 100;
		
		public function vo_orderTask()
		{
			
		}
	}
}